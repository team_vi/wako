/** \file io.h Input/output of compressed wavelet coefficients. **/
/* version 3.4.0

  Copyright (C) 2001-2007 Daniel Vollmer

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Vollmer <maven@maven.de>

*/

#ifndef _IO_H_
#define _IO_H_

#ifdef __cplusplus // C++
extern "C" {
#endif

#include "channel.h"

/** Version number as string. **/
#define wv_VERSION "3.5.0"
/** Version number as integer. **/
#define wv_VERNUM 0x0350

#define FILE_NAME_SIZE 256

typedef struct {
	int num_channels; /**< # of channels stored in the file **/
	int width; /**< Width of all the channels stored **/
	int height; /**< Height of all the channels stored **/
	unsigned char num_blocks; /**< # of coefficient blocks in the file **/
	unsigned char min_pel_store; /**< # of bytes needed in wv_pel (1 << n) **/
} t_wv_header;

int wv_query_header(const int NumChannels, t_wv_channel* Channel[]);
int wv_query_scheduler(const int NumChannels, t_wv_channel* Channel[], const double TargetMSE[], const int MaxBits, const int MinBits[], double EstimatedMSE[], int EstimatedBits[]);
int wv_encode(const int NumChannels, t_wv_channel* Channel[], const double TargetMSE[], const int MaxBits, const int MinBits[], t_bit_file* BF);
t_wv_header* wv_read_header(t_wv_header* Header, t_bit_file* BF);
int wv_decode(const t_wv_header* Header, int Reduction, t_wv_channel* Channel[wv_MAX_CHANNELS + 1], t_bit_file* BF);


#ifdef __cplusplus // C++
}
#endif

#endif /* _IO_H_ */
