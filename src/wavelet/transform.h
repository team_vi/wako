/** \internal \file transform.h Wavelet transform based on CDF(2,2). **/
/* version 3.4.0

  Copyright (C) 2001-2007 Daniel Vollmer

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Vollmer <maven@maven.de>

*/

#ifndef _TRANSFORM_H_INCLUDED
#define _TRANSFORM_H_INCLUDED

#ifdef __cplusplus // C++
extern "C" {
#endif

#include "config.h"

void wavelet_transform(wv_pel *dst, const int width, const int height, const int levels);
void inverse_wavelet_transform(wv_pel *dst, const int width, const int height, const int levels);
void estimate_error(const wv_pel *wavelet, const int width, const int owidth, const int height, const int oheight, int level, const int quant, double err[3]);

#ifdef __cplusplus // C++
}
#endif

#endif /* _TRANSFORM_H_ */
