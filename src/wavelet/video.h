/** \file video.h video file parameters and function declarations. **/
/* version 3.5.0

  Copyright (C) 2001-2017 Daniel Vollmer

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Vollmer <maven@maven.de>

*/

#ifndef _VIDEO_H_
#define _VIDEO_H_

#ifdef __cplusplus // C++
extern "C" {
#endif

#include <float.h> /* for DBL_MIN */
#include "channel.h"
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <math.h>

#define FILE_NAME_SIZE 256

#define SOME_PATH_MAX 1024


typedef struct
{
	double max_mse;
	wv_pel* data;
	int input_offset;
	int approx;
	int ycocg;
	char name[SOME_PATH_MAX];
} t_channel_params;

typedef struct
{
	const char* name_long;
	const char* name_short;
	const char* help;
	void* thing;
	unsigned char what; /* i=int, f/p=float, s=string, uppercase => global, lowercase => per channel */
} t_cmd_arg;

// stores yuv video parameters
typedef struct yuvVideo {
	int frame_size;
	int width;
	int height;
	int chwidth;
	int chheight;
	int vheight;
	int vwidth;
	int num_frames;
} yuv_video;

// struct to store YUV componenets of a frame
typedef struct yuvFrame {
	wv_pel* Y;
	wv_pel* Cb;
	wv_pel* Cr;
} yuv_frame;

// result of yuv video
typedef struct yuvAnalysis {
	int yuv_psnr;
	int yuv_ssim;
	int yuv_bits;
	int compr_ratio;
} yuv_analysis;

/**
 * Video reading/writing function declarations
 */
FILE* open_video(char *fname, const char* mode);
int close_video(FILE* in);
int read_frame(FILE* in, int frame_number, yuv_frame *vid_frame, yuv_video *vid_params);
int read_yuv(char* fbuff, yuv_frame *vid_frame, yuv_video *vid_params);
int encode_frame(int frame_number, yuv_frame *vid_frame, yuv_video * vid_params);
int decode_frame(int frame_number, yuv_frame *vid_frame, yuv_video * vid_params);
int encode_channel(int frame_number, char* enc_file, wv_pel* channel_data, int width, int height);
int decode_channel(int frame_number, char* enc_file, wv_pel *dec_channel, int width, int height);
int print_results(int frame_number);
int write_frame(FILE* out, int frame_number, yuv_frame *vid_frame, yuv_video * vid_params);
int write_compression_results(FILE *result, int num_frames);

#ifdef __cplusplus // C++
}
#endif

#endif /* _IO_H_ */
