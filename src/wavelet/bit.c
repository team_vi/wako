/* bit.c, version 3.4.0

  Copyright (C) 2001-2007 Daniel Vollmer

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Vollmer <maven@maven.de>

*/

#include <stdio.h>
#include <stdlib.h>
#include "bit.h"


/** The default size of the per-file bit-buffer (in bytes). An array
 * of this size is used as an in-memory buffer for bit-files that are read /
 * written from / to disk.
**/
#define BIT_BUFFER 1024


/** Opens a file as a bit-stream.
 * Reminiscient of \c fopen(), this function allows for reading and writing to
 * files on a bit-level basis from either disk or memory.
 * @param[in] name filename of the file to be opened. Alternatively, for memory-
 *   based read-access (i.e. <tt>mode == "rm"</tt>) pointer to the region to be
 *   read. When writing to memory and num_bits > 0, used as user-provided
 *   target buffer..
 * @param[in] mode string that specifies the operation. mode[0] = 'r' or 'w'
 *   (read or write), mode[1] = 'b' or 'm' (file or memory).
 * @param[in] num_bits is the number of bits available (or to be used from a
 *   file) when reading, or the maximum number of bits to write. Reads in
 *   excess of num_bits produces 0s, further writes are simply ignored.
 * @return the bit-file handle, or \c NULL on failure.
**/
t_bit_file* bit_open(char* name, const char *mode, int num_bits)
{
	t_bit_file* f = NULL;
	int num_bytes;

	num_bits = (num_bits <= 0) ? 0 : num_bits;
	num_bytes = (num_bits + 7) / 8;

	if (mode[1] == 'm')
	{ /* from / to mem */
		if ((mode[0] == 'r' && name && num_bits > 0) || mode[0] == 'w')
		{
			f = malloc(sizeof *f);
			f->file = NULL;
			f->own_buffer = (name == NULL) || (num_bits <= 0);

			if (num_bits > 0)
			{
				f->bufsize = f->bufleft = num_bytes;
				f->bits_left = num_bits;
			}
			else
			{
				f->bufsize = f->bufleft = BIT_BUFFER;
				f->bits_left = 1 << 30;
			}
			f->buffer = f->own_buffer ? malloc(f->bufsize * sizeof *f->buffer) : (unsigned char *)name;
		}
	}
	else
	{ /* file-based */
		FILE* file = fopen(name, mode);

		if (file)
		{
			f = malloc(sizeof *f);

			f->file = file;
			f->bits_left = (num_bits > 0) ? num_bits : (1 << 30);
			f->bufsize = f->bufleft = BIT_BUFFER;
			f->buffer = malloc(f->bufsize * sizeof *f->buffer);
			if (mode[0] == 'r')
			{
				f->bufleft = fread(f->buffer, sizeof *f->buffer, f->bufsize, f->file);
				if (f->bufleft < f->bufsize)
					f->bits_left = (f->bits_left < f->bufleft * 8) ? f->bits_left : f->bufleft * 8;
			}
		}
	}
	if (f)
	{
		f->idx = 0;
		f->initial_bits = f->bits_left;
		f->rwmode = mode[0];
		if (mode[0] == 'w')
		{
			f->mask = 8;
			f->current_byte = 0;
		}
		else
		{
			f->mask = 0x80;
			f->current_byte = f->buffer[f->idx];
		}
	}
	return f;
}


/** Flushes the contents of the memory-buffer to disk.
 * A partly filled byte is filled with 0-bits before the file-write occurs.
 * @param[in] f bit-stream file whose buffer is to be flushed.
**/
static void bit_flush(t_bit_file* f)
{
	if (f)
	{
		int num = f->idx;

		if (f->mask != 8 && f->mask != 0)
		{
			while (f->mask > 0)
			{
				f->current_byte <<= 1;
				f->mask--;
			}
			f->buffer[f->idx] = f->current_byte;
			num++;
		}
		if (f->file && num > 0)
		{
			wr_size += (num * (sizeof *f->buffer));
			// printf("--------------Size = %f\n", wr_size);
			fwrite(f->buffer, sizeof *f->buffer, num, f->file);
			f->idx = 0;
			f->mask = 8;
			f->bufleft = f->bufsize;
			f->current_byte = 0;
		}

	}
}


/** Close a bit-stream file opened with bit_open().
 * This function flushes all writes to disk / memory (if applicable) and then
 * deallocates the handle.
 * @param[in] f bit-stream file that is to be closed.
 * @param[out] mem when not \c NULL, it receives the pointer to the buffer
 *   (which is allocated automatically when writing to memory and \c NULL
 *   passed in originally).
 * @return number of bits read or written
**/
int bit_close(t_bit_file* f, unsigned char** mem)
{
	int ret = 0;

	if (f)
	{
		if (f->rwmode == 'w')
			bit_flush(f);

		if (f->file)
		{
			free(f->buffer);
			fclose(f->file);
		}
		else
		{
			if (mem)
				*mem = f->buffer;
			else if (f->rwmode != 'r' && f->own_buffer)
				free(f->buffer);
		}
		ret = f->initial_bits - f->bits_left;
		free(f);
	}
	return ret;
}


/** Frees an internally allocated memory buffer returned by bit_close().
 * @param[in] mem Pointer returned by bit_close() that was not originally
 *   provided by the user.
**/
void bit_free(unsigned char* mem)
{
	if (mem)
		free(mem);
}


/** Reads the given number of bits from a bit-stream file.
 * The file has to be openened in read-mode for this to work.
 * @param[in] num is the number of bits to be read. This value can be larger
 *   than 32, but then earlier bits are lost as they are shifted out of the
 *   variable.
 * @param[in] f bit-stream file from which to read (read-mode).
 * @return the bits as an unsigned integer (bits read first are in higher
 *   bit-positions).
**/
unsigned int bit_read(int num, t_bit_file* f)
{
	unsigned int out = 0;

	while (num > 0)
	{
		out <<= 1;
		num--;
		if (f->bits_left > 0 && f->idx < f->bufleft)
		{
			f->bits_left--;
			out |= (f->current_byte & f->mask) != 0x00;
			f->mask >>= 1;
			if (f->mask == 0x00)
			{ // read a byte
				f->mask = 0x80;
				if (++f->idx >= f->bufleft)
				{
					f->idx = 0;
					f->bufleft = 0;
					if (f->file)
					{
						f->bufleft = fread(f->buffer, sizeof *f->buffer, f->bufsize, f->file);
						if (f->bufleft < f->bufsize)
							f->bits_left = (f->bits_left < f->bufleft * 8) ? f->bits_left : f->bufleft * 8;
					}
				}
				f->current_byte = f->buffer[f->idx];
			}
		}
	}
	return out;
}


/** Reads a single bit from a bit-stream file.
 * The file has to be openened in read-mode for this to work. This is slightly
 * faster than using bit_read() for a single bit.
 * @param[in] f bit-stream file from which to read (read-mode).
 * @return the next bit from f as an unsigned integer.
**/
unsigned int bit_read_single(t_bit_file* f)
{
	unsigned int out = 0;

	if (f->bits_left > 0 && f->idx < f->bufleft)
	{
		f->bits_left--;
		out = (f->current_byte & f->mask) != 0x00;
		f->mask >>= 1;
		if (f->mask == 0x00)
		{ // read a byte
			f->mask = 0x80;
			if (++f->idx >= f->bufleft)
			{
				f->idx = 0;
				f->bufleft = 0;
				if (f->file)
				{
					f->bufleft = fread(f->buffer, sizeof *f->buffer, f->bufsize, f->file);
					if (f->bufleft < f->bufsize)
						f->bits_left = (f->bits_left < f->bufleft * 8) ? f->bits_left : f->bufleft * 8;
				}
			}
			f->current_byte = f->buffer[f->idx];
		}
	}
	return out;
}


/** Writes the given number of bits to a bit-stream file.
 * The file has to be openened in write-mode for this to work.
 * @param[in] bits are the bits to write. More significant bits are written
 *   before less significant ones.
 * @param[in] num is the number of bits to write. This value can be larger than
 *   32, but the respective bits are obviously written as 0-bits.
 * @param[in] f bit-stream file to which to write (write-mode).
 * @return the number of bits actually written.
**/
int bit_write(const unsigned int bits, int num, t_bit_file* f)
{
	int initial_left = f->bits_left;

	while (f->bits_left > 0 && num > 0)
	{
		unsigned int in_mask = 1 << (num - 1);

		f->current_byte = (f->current_byte << 1) | ((bits & in_mask) != 0);
		f->bits_left--;
		num--;
		if (--f->mask == 0x00)
		{ // filled a byte
			f->buffer[f->idx] = f->current_byte;
			f->current_byte = 0;
			f->mask = 8;
			if (++f->idx >= f->bufsize - ((num + 7) >> 3))
			{ // so that we have enough space for num bits (will be zero bits if > 32)
				if (f->file)
					bit_flush(f);
				else if (f->own_buffer)
				{
					f->bufsize *= 2;
					f->buffer = realloc(f->buffer, f->bufsize);
				}
			}
		}
	}
	return initial_left - f->bits_left;
}
