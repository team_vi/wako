/* main.c, version 3.4.0

  Copyright (C) 2001-2007 Daniel Vollmer

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Vollmer <maven@maven.de>

*/

/** \mainpage Wavelet Image Compression Library
\author Daniel Vollmer <code@maven.de>
\date \ref changelog "2007-07-10"
\attention <a href="http://www.maven.de/code/wavelet/">Online documentation</a>,
	<a href="http://www.maven.de/code/wavelet/Kompressor.zip">Kompressor.app</a> (Mac OS 10.4 Universal),
	<a href="http://www.maven.de/code/wavelet/wavelet.tar.bz2">source code</a>,
	and	<a href="http://www.maven.de/code/repos/wavelet/">Darcs repository</a>
\section kompressor Kompressor.app
There now is a graphical user-interface for Mac OS 10.4 called <em>Kompressor.app</em>,
which can be used to easily create, inspect and modify ".wko" files. It exercises
most features of the library and can open files from the <tt>wako</tt> command-line
compressor (of the same library version). Feedback on the application and its UI
is much appreciated, it's my first proper try of a Mac OS / Cocoa application.

\section main_cmd Example code
The included command-line application (in main.c) (badly) illustrates the use of
the rest of the library. It can be used to compress and decompress .raw images
and handles color-space conversion by remembering the colorspace of each channel
in its retained data.

To build an executable on Mac OS or Linux use the following:
<tt>gcc -O2 -W -Wall -lm *.c -o wako</tt>

Many command-line parameters (such as \c -offset and \c -mse for example) can be
given repeatedly and are then set for the current channel and onwards until the
option is changed again, for example <tt>./wako -y 1 -i test_r.raw -mse 2.0 -i
test_g.raw -mse 4.0 -i test_b.raw</tt> uses a maximum MSE of 2.0 for the Y
channel (as we specified color-space conversion with \c -y), and 4.0 for the two
color channels.

\section main_own Usage in your own program
The functions provided by the library usually work on channels, e.g. the red
channel of an RGB image, or the Y channel of a YCrCb image. By default, it
supports up to 16 channels in a single file, all of which have to have matching
dimensions. The compile-time configuration and types can be changed in
config.h to allow for higher bit-depths or more channels, etc.

\subsection own_comp Compression
To compress an image, you simply do the following:
-# Create a reorder-table that is used for permuting coefficients for the size
   of channels you are about to compress with wv_create_reorder_table().
-# Initialize each channel with wv_channel_compact() to obtain a t_wv_channel
   handle. You can then free the original data.
-# Free the reorder-table with wv_free_reorder_table() if you won't
   need it again for channels of the same size.
-# Optionally, request information on whether the given settings will work and
   how much error the would approximately yield with wv_query_scheduler().
-# Write the channels to a t_bit_file with wv_encode(), giving the requested
   maximum errors and / or size as parameters.
-# Free the t_wv_channel handles with wv_channel_free().

\subsection own_decomp Decompression
To decompress an image again, you essentially reverse the above process:
-# Read a set of t_wv_channel from a t_bit_file with wv_decode(). You can either
   manually read the header (if you want to know the dimensions beforehand for
   example) and provide it to wv_decode(), or let wv_decode() read it.
-# Create a reorder-table with wv_create_reorder_table() with the dimensions
   from the t_wv_channel (unless you already have a table for that size or one
   for an exact multiple of the dimensions used).
-# Use wv_channel_uncompact() to get the original image data back. You can then
   free the t_wv_channel handle.
-# Free the reorder-table with wv_free_reorder_table() (if not needed for
   anything else).

\section further Further Reading
For more information on how the code operate, have a look at the following
pages, whose order is similar to the actual coding process.
-# \ref overview
-# \ref transform
-# \ref reorder
-# \ref coding

\section license License
This code is licensed under a zlib-license, which can be found at the top of
each source-file.
**/

#include <time.h>
#include "video.h"

// // loads a raw gray-scale image and convert the unsigned chars to bytes
// static wv_pel* raw_load(const char *fname, const int header, const int width, const int height)
// {
// 	FILE* in = fopen(fname, "rb");
// 	wv_pel* out = NULL;
//
// 	if (in)
// 	{
// 		unsigned char* tmp_row;
// 		int i, j, xpitch, ypitch;
// 		int k = 0;
//
// 		xpitch = 1 << wv_log2i(width - 1);
// 		ypitch = 1 << wv_log2i(height - 1);
//
// 		out = malloc(xpitch * ypitch * sizeof *out);
// 		tmp_row = malloc(width * sizeof *tmp_row);
// 		fseek(in, header, SEEK_SET);
// 		for (i = 0; i < height; i++)
// 		{
// 			k = fread(tmp_row, sizeof *tmp_row, width, in);
// 			for (j = 0; j < k; j++)
// 				out[i * xpitch + j] = tmp_row[j]; // convert to wv_pel
// 		}
// 		if (k != width)
// 		{ // couldn't read all of it
// 			free(out);
// 			out = NULL;
// 		}
// 		else
// 		{
// 			for (i = 0; i < height; i++)
// 				for (j = width; j < xpitch; j++)
// 					out[i * xpitch + j] = out[i * xpitch + width - 1];
// 			for (i = height; i < ypitch; i++)
// 				memcpy(out + i * xpitch, out + (height - 1) * xpitch, xpitch * sizeof *out);
// 		}
// 		free(tmp_row);
// 		fclose(in);
// 	}
// 	return out;
// }
//
//
// // saves a raw gray-scale image from integers (takes max/min)
// static void raw_save(const char *fname, const wv_pel* src, const int width, const int height, const int pitch)
// {
// 	FILE* out = fopen(fname, "wb");
//
// 	if (out)
// 	{
// 		unsigned char *tmp_row;
// 		int i, j;
//
// 		tmp_row = malloc(width * sizeof *tmp_row);
// 		for (i = 0; i < height; i++)
// 		{
// 			for (j = 0; j < width; j++)
// 				tmp_row[j] = MIN(255, MAX(0, src[i * pitch + j])); // convert to unsigned char
// 			fwrite(tmp_row, sizeof *tmp_row, width, out);
// 		}
// 		free(tmp_row);
// 		fclose(out);
// 	}
// }
//

#if defined(_MSC_VER)
#define strcasecmp stricmp
#endif

int main(int argc, char *argv[])
{
	FILE *in, *fout, *fresult, *ftemp;
	char in_file_name[FILE_NAME_SIZE];
	char out_file_name[FILE_NAME_SIZE];
	char result_csv_name[FILE_NAME_SIZE];
	int i;
	clock_t t1, t2;
	t1 = clock();
	yuv_video vid_params;
	yuv_frame vid_frame;
	yuv_frame dec_vid_frame;

	/**
	 * Initialize variables
	 */
	strcpy(in_file_name, "F:\\Workfolders\\Gilson\\pvc_codec\\streams\\original\\crowd0_380.yuv");
	strcpy(out_file_name, "F:\\Workfolders\\Gilson\\pvc_codec\\streams\\original\\crowd_bpp_1p0.yuv");
	strcpy(result_csv_name, "F:\\Workfolders\\Gilson\\pvc_codec\\streams\\original\\wako_compression_ratio.csv");

	vid_params.width = 1920;
	vid_params.height = 1080;
	vid_params.chwidth = vid_params.width/2;
	vid_params.chheight = vid_params.height;
	vid_params.vheight = vid_params.height;
	vid_params.vwidth = vid_params.width*16/6;
	vid_params.frame_size = vid_params.vwidth * vid_params.vheight;
	vid_params.num_frames = 300;

	fprintf(stdout, "Number of args = %d, size = %u\n", argc, sizeof argv);

	/**
	* Open the input file and the analysis file
	*/
	in = open_video(in_file_name, "rb");
	ftemp = open_video(out_file_name, "wb");
	close_video(ftemp);
	ftemp = open_video(result_csv_name, "w");
	close_video(ftemp);
	fout = open_video(out_file_name, "ab");
	fresult = open_video(result_csv_name, "a");

	/**
	* Read the video file and get the frame encode and decode it
	*/
	for(i=0; i < vid_params.num_frames; ++i) {
		wr_size = 0;
		read_frame(in, i, &vid_frame, &vid_params);
		encode_frame(i, &vid_frame, &vid_params);
		decode_frame(i, &dec_vid_frame, &vid_params);
		print_results(i);
		write_frame(fout, i, &dec_vid_frame, &vid_params);
		printf("======================= frame 1 write size = %f \n", wr_size);
		wr_size_array[i] = wr_size;
	}

	/**
	* Write the compression results to CSV file
	*/
	write_compression_results(fresult, vid_params.num_frames);
	close_video(in);
	close_video(fout);
	close_video(fresult);
	t2 = clock();
	float diff = ((float)(t2 - t1) / 1000000.0F) * 1000;
	printf("--Time taken seconds = %f", diff);
	getchar();
	return 0;
}
