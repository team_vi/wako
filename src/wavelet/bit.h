/** \file bit.h Bit-wise file or memory access. **/
/* version 3.4.0

  Copyright (C) 2001-2007 Daniel Vollmer

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Vollmer <maven@maven.de>

*/

#ifndef _BIT_H_
#define _BIT_H_

#ifdef __cplusplus // C++
extern "C" {
#endif

#include <stdio.h>

#define MAX_WRITE_ARRAY_SIZE 1000

/** Handle to a bit-stream file. **/
typedef struct {
	FILE* file; /**< Real file-handle when working with on-disk files **/
	unsigned char* buffer; /**< Buffer (or the input pointer) with data **/
	int idx; /**< Current position in buffer **/
	int bufsize; /**< Size of the buffer **/
	int bufleft; /**< How many valid bytes are currently in the buffer **/
	int bits_left; /**< Number of bits that are still available for us to read or have been written **/
	int initial_bits;
	unsigned char mask; /**< Used as mask for bit_read(), count for bit_write() **/
	unsigned char current_byte; /**< The current byte being read from / written to **/
	char rwmode; /**< Copy of the 1st entry of the mode-string from bit_open() **/
	char own_buffer; /**< Whether we own (as in "need to free") the buffer **/
} t_bit_file;

t_bit_file* bit_open(char* name, const char *mode, int num_bits);
int bit_close(t_bit_file* f, unsigned char** mem);
void bit_free(unsigned char* mem);
unsigned int bit_read(int num, t_bit_file* f);
unsigned int bit_read_single(t_bit_file* f);
int bit_write(const unsigned int bits, int num, t_bit_file* f);

double wr_size_array[MAX_WRITE_ARRAY_SIZE];
double wr_size;

#ifdef __cplusplus // C++
}
#endif

#endif /* _BIT_H_ */
