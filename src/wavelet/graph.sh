#!/bin/bash
# args = options passed to wako
# overwrites t.wko t.txt t2.txt t3.txt bpp.txt
# creates $i.txt for each channel with "bpp,rmse,psnr" and gnuplot.batch

step=0.01 # create data point every X bpp

# compress the file to t.wko
./wako $* -o t.wko >t.txt || exit 0

# extract resulting bpp (without header)
bpp=`cat t.txt | grep "bits / pixel" | egrep -o "[[:digit:]]+\.[[:digit:]]{2}"`

# increase by 0.01
bpp=`echo $bpp + $step | bc`

# remove text-file, because we append in the loop
rm t.txt
# now decompress for each 0.01 bpp increment
for i in `seq 0.01 $step $bpp`;
do
  ./wako $* -d t.wko -bpp $i >>t.txt
done

# create bpp file
seq 0.01 $step $bpp >bpp.txt
# create gnuplot.batch
echo -e 'set datafile separator ","\nset xlabel "bpp"\nset ylabel "rmse"\nset y2label "psnr"\nset ytics nomirror\nset y2tics\nset grid xtics noytics y2tics\nplot \\'>t3.txt

i=1
index=1
for arg in "$@"
do
  if [[ $arg = "-i" ]]
  then
    # extract channel
    cat t.txt | grep "$i\.\ ">$i.txt
    # extract rmse & psnr
    cut -d\  -f5,7 --output-delimiter=, $i.txt>t2.txt
    #merge bpp w/ rmse&psnr
    paste -d, bpp.txt t2.txt >$i.txt
    echo Channel $i \-\> $i.txt

	# now update the gnuplot.batch file
    echo \"$i.txt\" using 1:2 with lines title \"$i\" lt $i, \\ >> t3.txt
    echo \"$i.txt\" using 1:3 with lines notitle lt $i axis x1y2, \\ >> t3.txt
    let "i+=1"
  fi
  let "index+=1"
done
head -c -4 t3.txt > gnuplot.batch # remove the last newline and ", \" from gnuplot file
echo >>gnuplot.batch # append newline
# remove temporary files
rm t.wko t.txt t2.txt t3.txt bpp.txt
