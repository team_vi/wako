/** \internal \file coding.h Rice run-length (de-)coding of integers. **/
/* version 3.4.0

  Copyright (C) 2001-2007 Daniel Vollmer

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Vollmer <maven@maven.de>

*/

#ifndef _CODING_H_
#define _CODING_H_

#ifdef __cplusplus // C++
extern "C" {
#endif

#include "bit.h"
#include "config.h"

int rice_encode_size(const wv_pel* src, const int num, int k, int max_bp, int* bit_stat);
void rice_encode_plane(const wv_pel* src, const int num, int initial_k, const int cur_bp, unsigned int *refinement_cache, t_bit_file* bf);
int rice_decode_plane(wv_pel* dst, const int num, int k, int cur_bp, int rlr_bits_left, t_bit_file* bf);
void rice_decode_finalize(wv_pel* dst, const int num, const int last_bp);

#ifdef __cplusplus // C++
}
#endif

#endif /* _CODING_H_ */
