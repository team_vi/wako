/* reorder.c, version 3.4.0

  Copyright (C) 2001-2007 Daniel Vollmer

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Vollmer <maven@maven.de>

*/


/** \page reorder Reordering Process

\section reorderwhy Why would you?
In order to actually compress an image, we need to take advantage of redundancy
in the data. A very simple way to do this without a complicated prediction
mechanism, is run-length coding. In fact, we only run-length code subsequent 0s.
We still want to take advantage of the original 2D nature of the data, which
implies that coefficients close to each other have a higher probability of being
similar, but without the complication of a 2D context-model like quad- or zero-
trees.

Another reason for this reordering is that we can preserve the progressive
nature of wavelet coding by first writing the important coefficients and then
the less and less important ones (which we call the levels of the
decomposition).

\section reorderhow How do we reorder?
Then the coefficients are "reordered" into a one-dimensional array by \ref
transform "octave-bands" (and using a Hilbert space-filling curve within each
octave, first is a block of coefficients with HD (horizontal detail), followed
by a block of VD (vertical detail) coefficients, and finally a block the DD
(diagonal detail) coefficients.

Now, we are able to write the coefficients of each block to disk via
\ref coding.

\section reorderref References
- http://en.wikipedia.org/wiki/Space-filling_curve
- http://swissnet.ai.mit.edu/~jaffer/Geometry/HSFC.html
- John J. Bartholdi, III and Paul Goldsman, "Vertex-Labeling Algorithms for the
  Hilbert Spacefilling Curve", Software - Practice and Experience, 2001, Volume
  31-5
- Dr. Volker Markl, Frank Ramsak, "Universalschlüssel - Datenbankindexe in
  mehreren Dimensionen", 2001, c't 01/2001 - Magazin für Computertechnik
- Henrique Malvar, "Progressive Wavelet Coding of Images", 1999,
  IEEE Data Compression Conference March 1999

**/


#include <stdlib.h>
#include "reorder.h"
#include "utility.h"


/** Converts a Hilbert Curve derived key into the coordinates of a point.
 * This implementation is based on the paper "Vertex-Labeling Algorithms for the
 * Hilbert Spacefilling Curve" by John J. Bartholdi, III and Paul Goldsman. I
 * have combined the x and y coordinate into a single unsigned int. Also, due to
 * fixed point arithmetic, the coordinates of the points grow with each
 * iteration so that sufficient accuracy is maintained.
 * @param[in] key Hilbert derived key to be converted. In essence, we interpret
 *   every two bits of the key as a certain spatial subdivision of the
 *   unit-square, as described in the paper.
 * @param[in] numbits number of bits in the key (which has to be even). Only
 *   valid for even values smaller than or equal to 30 (as otherwise the point
 *   coordinates overflow or the key does not provide enough bits).
 * @param[in] a, b, c, d coordinates (combined x and y) for vertex labelling.
 * @return fixed point coordinates with a precision of numbits / 2 (combined).
**/
static unsigned int recurse_hilbert(unsigned int key, unsigned int numbits,
	unsigned int a, unsigned int b, unsigned int c, unsigned int d)
{
	if (numbits > 1)
	{
		unsigned char subcell;

		numbits -= 2; /* each subdivision decision takes 2 bits */
		subcell = (key >> numbits) & 3; /* namely these two (taken from the top) */

		switch (subcell)
		{ /* divide into subcells */
			case 0:
				return recurse_hilbert(key, numbits, a << 1, a + d, a + c, a + b);
			case 1:
				return recurse_hilbert(key, numbits, b + a, b << 1, b + c, b + d);
			case 2:
				return recurse_hilbert(key, numbits, c + a, c + b, c << 1, c + d);
			case 3:
				return recurse_hilbert(key, numbits, d + c, d + b, d + a, d << 1);
		}
	}
	else
	{ /* final result is the midpoint of the cell, i.e. (a + b + c + d) / 4 */
		unsigned int outx, outy;

		/* extract x from the lower 16 bits */
		outx = ((a & 0xffff) + (b & 0xffff) + (c & 0xffff) + (d & 0xffff) + 1) >> 2;
		/* extract y from the upper 16 bits */
		outy = ((a >> 16) + (b >> 16) + (c >> 16) + (d >> 16) + 1) >> 2;
		return (outy << 16) | outx;
	}
	return 0; /* make gcc happy */
}


/** \internal Computes the extents of actually used wavelet coefficients in a
 * block.
 * @param[in] width2 Next-bigger power of 2 to width.
 * @param[in] width Width of the input-image.
 * @param[in] height2 Next-bigger power of 2 to height.
 * @param[in] height Height of the input-image.
 * @param[in] shift Decomposition level.
 * @param[out] block_width Width of the used coefficients at requested level.
 * @param[out] block_height Height of the used coefficients at requested level.
**/
void get_block_dimensions(int width2, int width, int height2, int height, int shift, int *block_width, int *block_height)
{
	int w, h; /* width & height of the block / level */
	int uw, uh; /* unused width & height in block / level */

	/* size of the decomposition level */
	w = width2 >> shift;
	h = height2 >> shift;
	uw = (width2 - width) >> shift;
	uh = (height2 - height) >> shift;

	*block_width = MIN(w, w - uw + 1);
	*block_height = MIN(h, h - uh + 1);
}


static void construct_reorder_table(unsigned int *dst, const int size)
{
	int bits = wv_log2i(size) << 1; /* times 2 is squared in non-log */
	int i;

	for (i = 0; i < size * size; i++)
		*dst++ = recurse_hilbert(i, bits, 0x00000, 0x00001, 0x10001, 0x10000);
}


/** Creates a reorder-table that is used to permute 2D wavelet coefficients into
 * a 1D array. This reordering is based on the Hilbert space-filling curve.
 * Tables can be reused between images as long as it has been created for the
 * largest dimension. Only the smallest given dimension is actually relevant.
 * See \ref reorder for more information.
 * @param[in] MaxImageWidth Largest width of an image to be processed with by
 *   table.
 * @param[in] MaxImageHeight Largest height of an image to be processed with by
 *   table.
 * @return Reorder table, freed by wv_free_reorder_table().
**/
t_reorder_table* wv_create_reorder_table(const int MaxImageWidth, const int MaxImageHeight)
{
	t_reorder_table* t = NULL;
	unsigned int *all_the_ints = NULL;
	int size, levels, i;

	size = MIN(MaxImageWidth, MaxImageHeight);
	size = (size >= 2 && size < 4) ? 2 : (size + 1) >> 1; /* if no wavelet transform, biggest block has half dimensions of input image */
	size = 1 << wv_log2i(size - 1); /* Next bigger power of 2 */
	levels = wv_log2i(size) - 1; /* # of tables (can be < 0)*/

	t = malloc(sizeof *t);
	t->num_levels = MAX(0, levels);
	if (levels > 0)
	{ /* Sum[(2^i)^2, {i, n}] = 4/3 * (-1 + 2^n) * (1 + 2^n) */
		int num_entries = (4 * (-1 + (1 << levels)) * (1 + (1 << levels))) / 3;

		all_the_ints = malloc(num_entries * sizeof *all_the_ints);
	}
	for (i = 0; i < levels; i++)
	{
		int dimension = 2 << i; /* smallest table is 2 x 2 */

		t->level[i] = all_the_ints;
		construct_reorder_table(t->level[i], dimension);
		all_the_ints += dimension * dimension;
	}
	for (; i < wv_NUM_REORDER_LEVELS; i++)
		t->level[i] = NULL;
	return t;
}


/** Frees a reorder-table allocated with wv_create_reorder_table().
 * @param[in] Table Table to be freed.
**/
void wv_free_reorder_table(t_reorder_table *Table)
{
	if (Table)
	{
		if (Table->num_levels > 0)
			free(Table->level[0]); /* frees all levels, as the come from the same alloc */
		free(Table);
	}
}


static unsigned int reorder_block(wv_pel *dst, wv_pel* src, const unsigned int *idx, const unsigned int num, const int width2, const unsigned int coefficient_pitch, const unsigned int xlimit, const unsigned int ylimit)
{
	unsigned int count = 0;
	unsigned int i;
	unsigned int x, y;

	for (i = 0; i < num; i++)
	{
		x = *idx & 0xffff;
		y = *idx >> 16;
		if (x < xlimit && y < ylimit)
		{
			*dst++ = src[(y * width2 + x) * coefficient_pitch];
			count++;
		}
		idx++;
	}
	return count;
}

static unsigned int unreorder_block(wv_pel *src, wv_pel* dst, const unsigned int *idx, const unsigned int num, const int width2, const unsigned int coefficient_pitch, const unsigned int xlimit, const unsigned int ylimit)
{
	unsigned int count = 0;
	unsigned int i;
	unsigned int x, y;

	for (i = 0; i < num; i++)
	{
		x = *idx & 0xffff;
		y = *idx >> 16;
		if (x < xlimit && y < ylimit)
		{
			dst[(y * width2 + x) * coefficient_pitch] = *src++;
			count++;
		}
		idx++;
	}
	return count;
}


static int reorder_level(t_reorder_function func, wv_pel *dst, wv_pel* src, const t_reorder_table* table, const int width, const int height, const unsigned int coefficient_pitch, const int level)
{
	int width2 = 1 << wv_log2i(width - 1); /* next bigger pow2 */
	int height2 = 1 << wv_log2i(height - 1); /* next bigger pow2 */
	int reorder_level = wv_log2i(MIN(width2, height2) >> level) - 2;
	int block_size = 2 << reorder_level; /* dimension of the used reorder table */
	int count = 0;
	int level_width, level_height;
	int i;

	wv_ASSERT(reorder_level < table->num_levels, "Not enough levels in reorder table: Initialise table for the proper dimensions");
	get_block_dimensions(width2, width, height2, height, level, &level_width, &level_height);

	/* we only have to loop in 1D (because if it were in both x & y, we'd just use a larger table) */
	if (width > height)
	{ /* loop horizontally */
		int bx, num_bx = level_width / block_size;
		unsigned int xlimit = level_width - num_bx * block_size;

		for (bx = 0; bx < num_bx; bx++)
		{
			i = func(dst, src + bx * block_size * coefficient_pitch, table->level[reorder_level], block_size * block_size, width2, coefficient_pitch, block_size, level_height);
			dst += i;
			count += i;
		}
		if (xlimit > 0)
		{
			i = func(dst, src + bx * block_size * coefficient_pitch, table->level[reorder_level], block_size * block_size, width2, coefficient_pitch, xlimit, level_height);
			dst += i;
			count += i;
		}
	}
	else
	{ /* loop vertically */
		int by, num_by = level_height / block_size;
		unsigned int ylimit = level_height - num_by * block_size;

		for (by = 0; by < num_by; by++)
		{
			i = func(dst, src + by * width2 * block_size * coefficient_pitch, table->level[reorder_level], block_size * block_size, width2, coefficient_pitch, level_width, block_size);
			dst += i;
			count += i;
		}
		if (ylimit > 0)
		{
			i = func(dst, src + by * width2 * block_size * coefficient_pitch, table->level[reorder_level], block_size * block_size, width2, coefficient_pitch, level_width, ylimit);
			dst += i;
			count += i;
		}
	}
	return count;
}


void reorder(wv_pel *dst, wv_pel* src, const t_reorder_table* table, const int width, const int height, const int unreorder)
{
	int min_dimension = MIN(width, height);
	int i;

	if (min_dimension < 2)
	{ /* we haven't done a transform, and one of the input dimensions is 1; so we do a straight copy */
		int num = MAX(width, height);

		for (i = 0; i < num; i++)
			*dst++ = src[i];
	}
	else
	{ /* we use the reorder table */
		t_reorder_function func = reorder_block; /* default to reorder */
		int num_levels = MAX(0, wv_log2i(min_dimension - 1) - 1);
		num_levels = 3;
		int width2 = 1 << wv_log2i(width - 1); /* next bigger pow2 */
		unsigned int pitch = 1 << num_levels; /* between wavelet coefficients of the same block */

		if (unreorder != 0)
		{ /* swap src and dst */
			wv_pel *tmp = src;

			src = dst;
			dst = tmp;
			func = unreorder_block;
		}

		/* output base average (or the pure data if too small for wavelet transform) */
		dst += reorder_level(func, dst, src, table, width, height, pitch, num_levels);
		for (i = num_levels; i > 0; i--)
		{
			unsigned int halfpitch = pitch >> 1;

			dst += reorder_level(func, dst, src + halfpitch, table, width, height, pitch, i); /* HD */
			dst += reorder_level(func, dst, src + halfpitch * width2, table, width, height, pitch, i); /* VD */
			dst += reorder_level(func, dst, src + halfpitch * (width2 + 1), table, width, height, pitch, i); /* DD */
			pitch = halfpitch; /* halve the pitch for each level we go up */
		}
	}
}
