/** \file channel.h Single-channel processing (prepare for en-/decoding). **/
/* version 3.4.0

  Copyright (C) 2001-2007 Daniel Vollmer

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Vollmer <maven@maven.de>

*/

#ifndef _CHANNEL_H_
#define _CHANNEL_H_

#ifdef __cplusplus // C++
extern "C" {
#endif

#include "bit.h"
#include "config.h"
#include "reorder.h"

/** Maximum amount of blocks we can have (which is for a 32768x32768 image). **/
#define wv_MAX_BLOCKS (1 + 12*3)
/** Amount of bits needed to store wv_MAX_BLOCKS (as we know it's 1 + multiple of 3) **/
#define wv_MAX_BLOCK_STORE 4

/** Information about a single (or multiple agglomerated) bit-planes, needed for scheduling. **/
typedef struct {
	double improvement; /**< Improvement in mean-square error for this bitplane **/
	double improvement_per_bit; /**< Improvement per bit **/
	int num_bits; /**< # Bits needed for this bitplane (including block index) **/
	unsigned char count; /**< Agglomeration count of how many bitplanes we've combined **/
} t_wv_bitplane;

/** Information about a block of wavelet coefficients.
 * In this context, a \em block refers to the detail coefficients of a
 * particular wavelet decomposition level. For example, a 512x512 image is first
 * decomposed into a 256x256 average (this is then again decomposed recursively)
 * and 3 256x256 detail coefficients. These 3 256x256 detail coefficients make
 * up a so-called \em block.
 */
typedef struct {
	t_wv_bitplane* plane; /**< (Temporary) Array of bitplanes (0 ^= highest bitplane!) **/
	int	offset; /**< Where our coefficients start in t_wv_channel.reordered_channel **/
	int	size; /**< Number of relevant wavelet coefficients in this block **/
	unsigned char num_planes; /**< Number of bit-planes needed to encode the block **/
} t_wv_block;

/** Entry in the scheduler about the order in which we write blocks. **/
typedef struct {
	wv_fixed_point improvement; /**< Improvement in mean-square error for this entry **/
	int num_bits; /**< # of bits needed for this block (including block index) **/
	unsigned char count; /**< Agglomeration count of how many bitplanes we've combined **/
	unsigned char b; /**< Index of the block to write next **/
	unsigned char p; /**< Bit-plane to be written next (0 ^= highest) **/
} t_wv_schedule_entry;

/** Channel of wavelet coefficients to be compressed.
 * This data structure stores its own private (transformed & reordered)
 * version of the image data.
**/
typedef struct {
	int width; /**< Width of the input-image (not necessarily pow2) **/
	int height; /**< Height of the input-image (not necessarily pow2) **/
	int num_schedule_entries; /**< # of entries in the schedule **/
	t_wv_schedule_entry* schedule; /**< The actual schedule **/
	wv_pel* reordered_channel; /**< Reordered wavelet coefficients **/
	t_wv_block* block; /**< Block-Array **/
	unsigned char num_blocks; /**< # of coefficient blocks **/
	unsigned char schedule_fractional_bits; /**< Number of fractional bits in the improvement values **/
	unsigned char data_format; /**< User-data stored in the compressed file **/
	unsigned char min_pel_store; /**< Indicates minimum # of bytes needed in wv_pel (1 << min_pel_stor) **/
} t_wv_channel;

/** Progress callback for wv_channel_compact(). **/
typedef void (*wv_progress_function) (int Current, int End, void* UserData);


t_wv_channel* wv_channel_compact(const int Width, const int Height, const unsigned char DataFormat, wv_pel* Data,
	const int Approximate, const t_reorder_table* ReorderTable, wv_progress_function Progress, void* UserData);
void wv_channel_uncompact(const t_wv_channel *CC, const t_reorder_table* ReorderTable, wv_pel *Data);
void wv_channel_free(t_wv_channel* CC);

unsigned char wv_channel_get_data_format(const t_wv_channel *CC);

int wv_channel_get_width(const t_wv_channel *CC);
int wv_channel_get_height(const t_wv_channel *CC);

#ifdef __cplusplus // C++
}
#endif

#endif /* _CHANNEL_H_ */
