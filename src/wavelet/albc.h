/* albc.h, version 3.5.0

  Copyright (C) 2017 Gilson Varghese

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Gilson Varghese <gilsonvarghese7@gmail.com>

*/

#ifndef _ALBC_H_
#define _ALBC_H_

#include "channel.h"

void albc_encode(int num_channels, t_wv_channel* Channel[]);
void albc_decode(t_wv_channel* Channel[]);

#endif