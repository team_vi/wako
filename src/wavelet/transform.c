/* transform.c, version 3.4.0

  Copyright (C) 2001-2007 Daniel Vollmer

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Vollmer <maven@maven.de>

*/

/** \page transform Wavelet Transform
We start by computing a complete wavelet-transform using the Cohen-Daubechies-
Fevereau (2,2) biorthogonal wavelet (which incidentally seems to be the same as
used for lossless JPEG2000, but they call it a (5,3)?). For this, the image
needs dimensions which are a power of 2. To achieve this, I simply repeated the
last column and row of the image as necessary.

Doing the wavelet transform once, splits the image into average components as
well as horizontal (HD), vertical (VD) and diagonal detail (DD) coefficients.
Then we apply the transform to the resulting averages again and again, until the
smallest average block has either a width or height that is equal to 2.

The coefficients resulting from one round of the transform are in the same
so-called octave band (and are later on combined together via the \ref reorder
"reordering-process").

\section transformref References
- Peter Schröder, Wim Sweldens, "Course Notes: Wavelets in Computer Science",
  1996,  SIGGRAPH 96
- Wim Sweldens, "The Lifting Scheme: A New Philosophy in Biorthogoal Wavelet
  Construction", 1995, Wavelet Applications in Signal and Image Processing
- S. G. Mallard, "A Theory for Multiresolution Signal Decomposition: The Wavelet
  Representation", 1989, IEEE Transactions on Pattern Analysis and Machine
  Intelligence
**/

#include "transform.h"
#include "utility.h"


/** Detail coefficient for the CDF(2,2)-transform (lifting) **/
#define DETAIL(ptr, pitch) ((ptr)[0] -= ((ptr)[-(pitch)] + (ptr)[(pitch)]) / 2)
/** Last detail coefficient for the CDF(2,2)-transform (lifting) **/
#define DETAIL_LAST(ptr, pitch) ((ptr)[0] -= (ptr)[-(pitch)])
/** Average coefficient for the CDF(2,2)-transform (lifting). \Warning We store this at -pitch! **/
#define AVERAGE(ptr, pitch) ((ptr)[-(pitch)] += ((ptr)[0] + (ptr)[-2 * (pitch)]) / 4)
/** First average coefficient for the CDF(2,2)-transform (lifting). \Warning We store this at -pitch! **/
#define AVERAGE_FIRST(ptr, pitch) ((ptr)[-(pitch)] += (ptr)[0] / 2)

static void wavelet_row(wv_pel *dst, const int pitch, const int count)
{
	wv_pel *ptr;
	int i;

	ptr = dst + pitch; // odd

	DETAIL(ptr, pitch);
	AVERAGE_FIRST(ptr, pitch);
	for (i = count - 2; i > 0; i--)
	{
		ptr += 2 * pitch;
		DETAIL(ptr, pitch);
		AVERAGE(ptr, pitch);
	}
	ptr += 2 * pitch;
	DETAIL_LAST(ptr, pitch);
	AVERAGE(ptr, pitch);
}

static void wavelet_even_column_first(wv_pel *dst, const int pitch, const int rowpitch, const int count)
{
	wv_pel *rowptr;
	int i;

	rowptr = dst - rowpitch;

	for (i = 2 * count; i > 0; i--)
	{ // column (first)
		DETAIL(rowptr, rowpitch);
		AVERAGE_FIRST(rowptr, rowpitch);
		rowptr += pitch;
	}
}

static void wavelet_even_column(wv_pel *dst, const int pitch, const int rowpitch, const int count)
{
	wv_pel *rowptr;
	int i;

	rowptr = dst - rowpitch;

	for (i = 2 * count; i > 0; i--)
	{ // column (generic)
		DETAIL(rowptr, rowpitch);
		AVERAGE(rowptr, rowpitch);
		rowptr += pitch;
	}
}

static void wavelet_column_last(wv_pel *dst, const int pitch, const int rowpitch, const int count)
{
	wv_pel *rowptr;
	int i;

	rowptr = dst;

	for (i = 2 * count; i > 0; i--)
	{ // column (last)
		DETAIL_LAST(rowptr, rowpitch);
		AVERAGE(rowptr, rowpitch);
		rowptr += pitch;
	}
}


/** Computes the discrete wavelet decomposition of a 2D image.
 * This implementation uses a lifting implementation of the CDF(2,2) wavelet,
 * which is lossless for integer data. The transform is seperable in x and y,
 * but for performance reasons (cache), both are applied at essentially the same
 * time. The 2D transform splits the data into average and horizontal (HD),
 * vertical (VD) and diagonal (DD) detail coefficients, and can then be applied
 * again to the resulting average coefficients.
 * Inverse of inverse_wavelet_transform().
 * @param[in,out] dst Input data which is transformed into wavelet coefficients
 *   (as it is computed in place).
 * @param[in] width Width of data in dst, has to be divisble by 2^(levels+1).
 * @param[in] height Height of data in dst, has to be divisble by 2^(levels+1).
 * @param[in] levels How often the transform is applied to the resulting average
 *   coefficients. Largest valid value is wv_log2i(MIN(width, height)) - 2.
**/
void wavelet_transform(wv_pel *dst, const int width, const int height, const int levels)
{
	int cl;

	for (cl = 0; cl < levels; cl++)
	{
		int wmid = width >> (cl + 1);
		int pitch = 1 << cl, rowpitch = width << cl;
		int i;

		/* precompute the first three rows */
		wavelet_row(dst, pitch, wmid);
		wavelet_row(dst + pitch * width, pitch, wmid);
		wavelet_row(dst + 2 * pitch * width, pitch, wmid);
		/* do the column transform on 1st two rows */
		wavelet_even_column_first(dst + 2 * pitch * width, pitch, rowpitch, wmid);
		for (i = 3 * pitch; i < height - pitch; i += 2 * pitch)
		{
			wavelet_row(dst + i * width, pitch, wmid); // odd row
			wavelet_row(dst + (i + pitch) * width, pitch, wmid); // even row
			wavelet_even_column(dst + (i + pitch) * width, pitch, rowpitch, wmid); // previous two columns
		}
		wavelet_row(dst + i * width, pitch, wmid); // last (odd) row
		wavelet_column_last(dst + i * width, pitch, rowpitch, wmid); // and the last two columns
	}
}


/** Inverse first average coefficient for the CDF(2,2)-transform (lifting) **/
#define INV_AVERAGE_FIRST(ptr, pitch) ((ptr)[0] -= (ptr)[(pitch)] / 2)
/** Inverse average coefficient for the CDF(2,2)-transform (lifting).  **/
#define INV_AVERAGE(ptr, pitch) ((ptr)[0] -= ((ptr)[-(pitch)] + (ptr)[(pitch)]) / 4)
/** Inverse detail coefficient for the CDF(2,2)-transform (lifting). \Warning We store this at -pitch! **/
#define INV_DETAIL(ptr, pitch) ((ptr)[-(pitch)] += ((ptr)[-2 * (pitch)] + (ptr)[0]) / 2)
/** Inverse last detail coefficient for the CDF(2,2)-transform (lifting) **/
#define INV_DETAIL_LAST(ptr, pitch) ((ptr)[(pitch)] += (ptr)[0])

static void inverse_wavelet_row(wv_pel *dst, const int pitch, const int count)
{
	wv_pel *ptr;
	int i;

	ptr = dst; // even (average)

	INV_AVERAGE_FIRST(ptr, pitch);
	for (i = count - 1; i > 0; i--)
	{
		ptr += 2 * pitch;
		INV_AVERAGE(ptr, pitch);
		INV_DETAIL(ptr, pitch);
	}
	INV_DETAIL_LAST(ptr, pitch);
}

static void inverse_wavelet_column_first(wv_pel *dst, const int pitch, const int rowpitch, const int count)
{
	wv_pel *rowptr;
	int i;

	rowptr = dst;

	for (i = 2 * count; i > 0; i--)
	{ // column (first)
		INV_AVERAGE_FIRST(rowptr, rowpitch);
		rowptr += pitch;
	}
}

static void inverse_wavelet_column_even(wv_pel *dst, const int pitch, const int rowpitch, const int count)
{
	wv_pel *rowptr;
	int i;

	// column
	rowptr = dst;

	for (i = 2 * count; i > 0; i--)
	{ // column
		INV_AVERAGE(rowptr, rowpitch);
		INV_DETAIL(rowptr, rowpitch);
		rowptr += pitch;
	}
}

static void inverse_wavelet_column_last(wv_pel *dst, const int pitch, const int rowpitch, const int count)
{
	wv_pel *rowptr;
	int i;

	// column
	rowptr = dst;

	for (i = 2 * count; i > 0; i--)
	{ // column
		INV_DETAIL_LAST(rowptr, rowpitch);
		rowptr += pitch;
	}
}


/** Computes the inverse discrete wavelet decomposition of a 2D image.
 * This implementation uses a lifting implementation of the CDF(2,2) wavelet,
 * which is lossless for integer data. The transform is seperable in x and y,
 * but for performance reasons (cache), both are applied at essentially the same
 * time. The 2D transform splits the data into average and horizontal (HD),
 * vertical (VD) and diagonal (DD) detail coefficients, and can then be applied
 * again to the resulting average coefficients.
 * Inverse of wavelet_transform().
 * @param[in,out] dst Input wavelet coefficients which are transformed back into
 *   the original data (as it is computed in place).
 * @param[in] width Width of data in dst, has to be divisble by 2^(levels+1).
 * @param[in] height Height of data in dst, has to be divisble by 2^(levels+1).
 * @param[in] levels How often the transform was applied to the resulting average
 *   coefficients. Largest valid value is wv_log2i(MIN(width, height)) - 2.
**/
void inverse_wavelet_transform(wv_pel *dst, const int width, const int height, const int levels)
{
	int cl;

	for (cl = levels - 1; cl >= 0; cl--)
	{
		int wmid = width >> (cl + 1);
		int pitch = 1 << cl, rowpitch = width << cl;
		int i;

		// preculate first row (w/ vertical transform)
		inverse_wavelet_column_first(dst, pitch, rowpitch, wmid);
		for (i = 2 * pitch; i < height; i += 2 * pitch)
		{
			inverse_wavelet_column_even(dst + i * width, pitch, rowpitch, wmid);
			inverse_wavelet_row(dst + (i - 2 * pitch) * width, pitch, wmid);
			inverse_wavelet_row(dst + (i - pitch) * width, pitch, wmid);
		}
		inverse_wavelet_column_last(dst + (i - 2 * pitch) * width, pitch, rowpitch, wmid);
		inverse_wavelet_row(dst + (i - 2 * pitch) * width, pitch, wmid);
		inverse_wavelet_row(dst + (i - pitch) * width, pitch, wmid);
	}
}


/*static void quantize(wv_pel* dst, const int num, const int quant)
{
	if (quant > 1)
	{
		int i;

		for (i = 0; i < num; i++)
			*dst++ /= quant;
	}
}*/


/*int dead_zone_dequant(const int val, const int fac)
{ // has been multiplied by 10 to avoid floating point calculations entirely
	if (val > 0)
		return ((val * 10 + 5) * fac) / 10; // (val + 0.5) * fac
	else if (val < 0)
		return ((val * 10 - 5) * fac) / 10; // (val - 0.5) * fac
	else
		return 0;
}

static void dequantize(wv_pel* dst, const int num, const int quant)
{
	if (quant > 1)
	{
		int i;

		// start dequantizing w/ T1
		for (i = 0; i < num; i++)
			*dst++ = (wv_pel)dead_zone_dequant(*dst, quant);
	}
}*/

#define dead_zone_dequant(v, f) (((v) > 0) ? ((((v) << 1) + 1) * (f)) >> 1 : ((v) < 0) ? -((((-(v) << 1) + 1) * (f)) >> 1) : 0)
#define OFS(x, y) ((y) * pitch * width + (x) * pitch)

/** Estimates the error incurred by a particular quantization of wavelet
 * coefficients in the the original (untransformed) data. For the reasoning
 * behind the actual computations performed here, take a look the included
 * "wavelet_analysis.nb" Mathematica notebook.
 * @param[in] a Input wavelet coefficients. If this is NULL, then the error is
 *   computed based on the assumptions that the original data is evenly
 *   distributed and that quantizing the wavelet coefficients results in the
 *   same error as quantizing the original data.
 * @param[in] width Width of data in a, has to be divisble by 2^(levels+1).
 * @param[in] owidth Original width of data in a (not neccessarily pow2).
 * @param[in] height Height of data in a, has to be divisble by 2^(levels+1).
 * @param[in] oheight Original height of data in a (not neccessarily pow2).
 * @param[in] level Which level of the decomposition to work on.
 * @param[in] quant Quantizer (i.e. value by which to divide the data in a).
 * @param[out] err Estimated mean square errors (for average or HD, VD, DD).
**/
void estimate_error(const wv_pel *wavelet, const int width, const int owidth, const int height, const int oheight, int level, const int quant, double err[3])
{
	double diff;
	/* how big is a block ("level") of coefficients in x and y */
	int block_size_x = width >> level;
	int block_size_y = height >> level;
	int unused_size_x, unused_size_y, pitch;
	int y, x, ofs;

	err[0] = err[1] = err[2] = 0.0;

	if (quant <= 1)
		return;

	if (!wavelet)
	{ /* estimate errors independently of actual data */
		/* For uniform data and quantizer q, we have errors of the form
		  [-q/2, -q/2 + 1, ..., 0, ..., q/2 - 1] repeating endlessly. Also,
		  we report the mean-squared error, which means we have to square
		  each indivudal term. Because they repeat, it is enough to sum one
		  "sub-series" and divide by the number of terms in it to get the
		  average. */
		double numq = quant / 2.0 - 1.0; /* # of terms in +ive half of series */
		double est_error;

		/* First term squared and divided by the number of terms (which is quant) */
		est_error = quant / 4.0;

		/* Use the fact that Sum[i^2, {i, n}] = 1/6 n (1 + n) (1 + 2n), but
		  only divide by 3 because of -ive and +ive halves of the series.
		  Also divide by quant (# of total terms) to get the average. */
		est_error += (numq * (1 + numq) * (1 + 2 * numq)) / (3.0 * quant);

		err[0] = err[1] = err[2] = est_error;
	}
	else
	{
		#define ERR(v) ((v) - dead_zone_dequant((v) / quant, quant))

		if (block_size_x < 2 || block_size_y < 2)
		{ /* initial average block */
			level--;
			pitch = 1 << level;
			block_size_x = width >> level;
			block_size_y = height >> level;
			unused_size_x = (width - owidth) >> level;
			unused_size_y = (height - oheight) >> level;
			for (y = 0; y < block_size_y - unused_size_y; y++)
			{
				for (x = 0; x < block_size_x - unused_size_x; x++)
				{
					ofs = OFS(x, y);
					diff = ERR(wavelet[ofs]);
					err[0] += diff * diff;
				}
			}
			err[0] /= (block_size_x - unused_size_x) * (block_size_y - unused_size_y);
		}
		else
		{ /* detail block */
			int HDofs, VDofs;

			unused_size_x = (width - owidth) >> level;
			unused_size_y = (height - oheight) >> level;
			pitch = 1 << level;
			HDofs = (pitch >> 1);
			VDofs = (pitch >> 1) * width;

			#define HD(ofs) wavelet[(ofs) + HDofs]
			#define VD(ofs) wavelet[(ofs) + VDofs]
			#define DD(ofs) wavelet[(ofs) + HDofs + VDofs]

			for (y = 0; y < block_size_y - unused_size_y; y++)
			{
				/* horizontal detail index (HD) */
				#define H 0
				/* vertical detail index (VD) */
				#define V 1
				/* diagonal detail index (DD) */
				#define D 2
				/* left index (when used for y ^= top) */
				#define L 0
				/* center index */
				#define C 1
				/* right index (when used for y ^= bottom) */
				#define R 2

				/* y index left, right (actually top & bottom) */
				int yli = MAX(y - 1, 0), yri = MIN(y + 1, block_size_y - 1);
				/* store errors incurred (for each HD, VD, DD a 3x3 stencil) */
				float e[D + 1][R + 1][R + 1];

				/* We copy the error in the coefficients into this array and then
				only update the ones we need (after shifting them). This way
				the inner loop only references fixed offsets into errs. */
				ofs = OFS(0, yli);
				e[H][L][C] = e[H][L][R] = ERR(HD(ofs));
				e[V][L][C] = e[V][L][R] = ERR(VD(ofs));
				e[D][L][C] = e[D][L][R] = ERR(DD(ofs));
				ofs = OFS(0, y);
				e[H][C][C] = e[H][C][R] = ERR(HD(ofs));
				e[V][C][C] = e[V][C][R] = ERR(VD(ofs));
				e[D][C][C] = e[D][C][R] = ERR(DD(ofs));
				ofs = OFS(0, yri);
				e[H][R][C] = e[H][R][R] = ERR(HD(ofs));
				e[V][R][C] = e[V][R][R] = ERR(VD(ofs));
				e[D][R][C] = e[D][R][R] = ERR(DD(ofs));

				for (x = 0; x < block_size_x - unused_size_x; x++)
				{
					int xri = MIN(x + 1, block_size_x - 1); /* right */

					/* shift horizontal detail error left */
					e[H][L][L] = e[H][L][C]; e[H][C][L] = e[H][C][C]; e[H][R][L] = e[H][R][C];
					e[H][L][C] = e[H][L][R]; e[H][C][C] = e[H][C][R]; e[H][R][C] = e[H][R][R];
					/* shift vertical detail error left */
					e[V][L][L] = e[V][L][C]; e[V][C][L] = e[V][C][C]; e[V][R][L] = e[V][R][C];
					e[V][L][C] = e[V][L][R]; e[V][C][C] = e[V][C][R]; e[V][R][C] = e[V][R][R];
					/* shift diagonal detail error left */
					e[D][L][L] = e[D][L][C]; e[D][C][L] = e[D][C][C]; e[D][R][L] = e[D][R][C];
					e[D][L][C] = e[D][L][R]; e[D][C][C] = e[D][C][R]; e[D][R][C] = e[D][R][R];

					/* fill in the new errors on the right side */
					ofs = OFS(xri, yli);
					e[H][L][R] = ERR(HD(ofs)); e[V][L][R] = ERR(VD(ofs)); e[D][L][R] = ERR(DD(ofs));
					ofs = OFS(xri, y);
					e[H][C][R] = ERR(HD(ofs)); e[V][C][R] = ERR(VD(ofs)); e[D][C][R] = ERR(DD(ofs));
					ofs = OFS(xri, yri);
					e[H][R][R] = ERR(HD(ofs)); e[V][R][R] = ERR(VD(ofs)); e[D][R][R] = ERR(DD(ofs));

					/* HD contributions */
					diff = (e[H][C][L] + e[H][C][C]) / 4.0;
					err[0] += diff * diff; /* x even, y even */
					diff = (6 * e[H][C][C] - e[H][C][L] - e[H][C][R]) / 8.0;
					err[0] += diff * diff; /* x odd, y even */
					diff = (e[H][C][L] + e[H][C][C] + e[H][R][L] + e[H][R][C]) / 8.0;
					err[0] += diff * diff; /* x even, y odd */
					diff = (6 * (e[H][C][C] + e[H][R][C]) - e[H][C][L] - e[H][C][R] - e[H][R][L] - e[H][R][R]) / 16.0;
					err[0] += diff * diff; /* x odd, y odd */
					/* VD contributions */
					diff = (e[V][L][C] + e[V][C][C]) / 4.0;
					err[1] += diff * diff; /* x even, y even */
					diff = (e[V][L][C] + e[V][L][R] + e[V][C][C] + e[V][C][R]) / 8.0;
					err[1] += diff * diff; /* x odd, y even */
					diff = (6 * e[V][C][C] - e[V][L][C] - e[V][R][C]) / 8.0;
					err[1] += diff * diff; /* x even, y odd */
					diff = (6 * (e[V][C][C] + e[V][C][R]) - e[V][L][C] - e[V][L][R] - e[V][R][C] - e[V][R][R]) / 16.0;
					err[1] += diff * diff; /* x odd, y odd */
					/* DD contributions */
					diff = (e[D][L][L] + e[D][L][C] + e[D][C][L] + e[D][C][C]) / 16.0;
					err[2] += diff * diff; /* x even, y even */
					diff = (e[D][L][L] + e[D][L][R] + e[D][C][L] + e[D][C][R] - 6 * (e[D][L][C] + e[D][C][C])) / 32.0;
					err[2] += diff * diff; /* x odd, y even */
					diff = (e[D][L][L] + e[D][L][C] + e[D][R][L] + e[D][R][C] - 6 * (e[D][C][L] + e[D][C][C])) / 32.0;
					err[2] += diff * diff; /* x even, y odd */
					diff = (e[D][L][L] + e[D][L][R] + e[D][R][L] + e[D][R][R] - 6 * (e[D][L][C] + e[D][C][L] + e[D][C][R] + e[D][R][C]) + 36 * e[D][C][C]) / 64.0;
					err[2] += diff * diff; /* x odd, y odd */
				}
				#undef H
				#undef V
				#undef D
				#undef L
				#undef C
				#undef R
			}
			#undef HD
			#undef VD
			#undef DD
			/* as we have computed errors for 4 * as many pixels */
			diff = (block_size_x - unused_size_x) * (block_size_y - unused_size_y) * 4;
			err[0] /= diff; err[1] /= diff; err[2] /= diff;
		}
		#undef ERR
	}
}

#undef OFS
#undef dead_zone_dequant
