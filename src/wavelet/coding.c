/* coding.c, version 3.4.0

  Copyright (C) 2001-2007 Daniel Vollmer

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Vollmer <maven@maven.de>

*/


/** \page coding Coding Process

\section codewhat What is coded?
We have a one-dimensional array of data (usually consisting of quantized wavelet
coefficients) that we want to store efficiently, but also in a progressive
manner. This is achieved by coding the bit-planes from the top down, e.g. first
we code bit 8 of all coefficients, then bit 7 of all coefficients, and so on
until we have coded all the information.

\section codehow How is it coded?
Due to the wavelet transform  we hope to have reduced most coefficients to small
values (or 0 in the ideal case), thus we only run-length code runs of
consecutive 0 bits. Each 0 bit in the coded stream represents 2^\em k zeros,
whereas a 1 in the coded stream is always followed by \em k bits (denoting some
value \em n) and a sign bit. This is interpreted as \em n 0 bits followed by a
single 1 bit (and the value to which the 1 bit belongs as having that particular
sign).

Now, two additional things are done:
- The value of \em k does not have to be fixed. It can also be adaptive, as long
  as the encoder and decoder always make the same choice of \em k. We increase
  \em k after emitting a 0 bit and decrease it after emitting a 1 bit.
- Secondly, we make a distinction between significant bits and so-called
  refinement bits. Significant bits are those whose most significant bit has
  \em not already been sent / coded. Consequently, refinement bits are the ones
  for which more significant 1 bits have already been coded. The significant
  bits have a high probability of being 0, the refinement bits in contrast only
  have a probability close to 0.5 of being 0. Thus, we ignore the refinement
  bits when run-length coding zero-runs of the significant bits (as the decoder
  is aware for which coefficients this is the case) and embed the "ignored"
  refinement bits into the bit-stream without any coding after writing the next
  1 bit.


\section coderef References
- http://en.wikipedia.org/wiki/Rice_coding
- Henrique Malvar, "Progressive Wavelet Coding of Images", 1999,
  IEEE Data Compression Conference March 1999
**/

#include <stdlib.h>
#include <string.h>
#include "coding.h"
#include "utility.h"


/** The smallest allowed value for the adaptive parameter k. During the
 * run-length coding process, any run of 0s smaller than 2^RICE_MIN_K will have
 * to be escaped.
**/
#define RICE_MIN_K 0
/** The largest allowed value for the adaptive parameter k. This means the
 * longest run of 0s we can represent is 2^RICE_MAX_K.
 * \Warning This cannot be larger than 30 as we write up to 2 + k bits in a
 * single bit_write(), and the argument to that may not exceed 32 bits.
**/
#define RICE_MAX_K 30


/** \internal Encode a bitplane of a block of coefficients using adaptive Rice
 * coding. Rice coding is a variant of Golomb coding. For more information see
 * \ref coding.
 * @param[in] src coefficients (or simply integers) to be coded.
 * @param[in] num number of coefficients.
 * @param[in] k initial value of \em k for the adaptive Rice coding; has to be
 *   consistent between encoding and decoding!
 * @param[in] cur_bp Which bitplane to code (0 ^= least significant ), which
 * assumes that all the preceding (i.e. higher) planes have already been coded.
 * @param[in] refinement_cache A (temporary) store refinement bits which we
 *   flush (uncoded) when a 0 run finishes; has to provide (num + 31) / 32 ints.
 * @param[in] bf file to write coded data to.
**/
void rice_encode_plane(const wv_pel* src, const int num, int k, const int cur_bp, unsigned int *refinement_cache, t_bit_file* bf)
{
	int num_zeros = 0, num_refinement = 0;
	int idx, i;
	wv_pel mask = (wv_pel)1 << cur_bp; /* extract the bit for the current bitplane */
	wv_pel non_ref = ((wv_pel)2 << cur_bp) - 1; /* largest possible non-refinement value */

	k = MAX(RICE_MIN_K, MIN(RICE_MAX_K, k)); /* make sure k is in valid range */

	for (idx = 0; idx < num; idx++)
	{ /* each coefficient */
		wv_pel cur = abs(src[idx]);

		if (cur <= non_ref)
		{ /* not refinement */
			if ((cur & mask) == 0)
				num_zeros++;
			else
			{ /* a (non-refinement) 1 bit */
				i = 0; /* amount of 0 bits we are about to write */
				while (num_zeros >= (1 << k))
				{ /* count the # of accumulated zeros bits */
					i++;
					num_zeros -= (1 << k);
					k = MIN(RICE_MAX_K, k + 1);
				}
				bit_write(0, i, bf);

				/* write 1 + left over 0s + sign */
				bit_write((1 << (k + 1)) | (num_zeros << 1) | (src[idx] < 0), 1 + k + 1, bf);
				num_zeros = 0;
				k = MAX(RICE_MIN_K, k - 1);

				if (num_refinement > 0)
				{ /* and flush the accumulated refinement bits */
					for (i = 0; i < num_refinement >> 5; i++)
						bit_write(refinement_cache[i], 32, bf);
					bit_write(refinement_cache[num_refinement >> 5], num_refinement & 31, bf);
					num_refinement = 0;
				}
			}
		}
		else
		{ /* refinement bit (i.e. an entry whose MSB has already been sent) */
			refinement_cache[num_refinement >> 5] = (refinement_cache[num_refinement >> 5] << 1) | ((cur & mask) != 0);
			num_refinement++;
		}
	} /* each coefficient */

	i = 0; /* amount of 0 bits we are about to write */
	while (num_zeros > 0)
	{ /* we may indicate too many zeros, but in the decoder we know when a bitplane is done */
		i++;
		num_zeros -= (1 << k);
		k = MIN(RICE_MAX_K, k + 1);
	}
	bit_write(0, i, bf); /* and finally write the 0s */

	for (i = 0; i < num_refinement >> 5; i++)
		bit_write(refinement_cache[i], 32, bf);
	bit_write(refinement_cache[num_refinement >> 5], num_refinement & 31, bf);
}


/** \internal Count how many bits would have been used to encode a block of
 * coefficients using adaptive Rice coding. This routine is a modified version
 * of rice_encode_plane() that does not do any actual writing, but returns the
 * number of bits that would have been used to encode each bitplane, which is
 * useful in creating the schedule and faster than writing to a temporary file.
 * @param[in] src coefficients (or simply integers) to be coded.
 * @param[in] num number of coefficients.
 * @param[in] initial_k initial value of \em k for the adaptive Rice coding.
 *   Has to be consistent between encoding and decoding!
 * @param[in] num_bp number of bitplanes in src.
 * @param[out] bit_stat receives the number of bits needed to encode the data
 *   thus far for each bitplane. Has to have space for at least max_bp
 *   entries.
 * @return total number of bits "written".
**/
int rice_encode_size(const wv_pel* src, const int num, int initial_k, int num_bp, int* bit_stat)
{
	int bits_written = 0;

	if (num == 0)
		return 0;

	if (num_bp > 0)
	{
		int cur_bp;

		initial_k = MAX(RICE_MIN_K, MIN(RICE_MAX_K, initial_k)); /* make sure k is in valid range */
		for (cur_bp = num_bp - 1; cur_bp >= 0; cur_bp--)
		{ /* each coded bitplane */
			int num_zeros = 0, num_refinement = 0;
			int k = initial_k;
			int idx;
			wv_pel mask = (wv_pel)1 << cur_bp; /* extract the bit for the current bitplane */
			wv_pel ref = ((wv_pel)2 << cur_bp) - 1; /* largest possible non-refinement value */

			for (idx = 0; idx < num; idx++)
			{ /* each coefficient */
				wv_pel cur = abs(src[idx]);

				if (cur <= ref)
				{ /* not refinement */
					if ((cur & mask) == 0)
						num_zeros++;
					else
					{ /* a (non-refinement) 1 bit */
						while (num_zeros >= (1 << k))
						{ /* count the # of accumulated zeros bits */
							bits_written++;
							num_zeros -= (1 << k);
							k = MIN(RICE_MAX_K, k + 1);
						}
						bits_written += 1 + k + 1; /* '1' + # of '0's + sign */
						num_zeros = 0;
						k = MAX(RICE_MIN_K, k - 1);

						bits_written += num_refinement; /* flush the accumulated refinement bits */
						num_refinement = 0;
					}
				}
				else
				{ /* refinement bit (i.e. an entry whose MSB has already been sent) */
					num_refinement++;
				}
			} /* each coefficient */

			while (num_zeros > 0)
			{ /* we may indicate too many zeros, but in the decoder we know when a bitplane is done */
				bits_written++;
				num_zeros -= (1 << k);
				k = MIN(RICE_MAX_K, k + 1);
			}

			bits_written += num_refinement;
			if (bit_stat)
				bit_stat[num_bp - 1 - cur_bp] = bits_written;
		} /* each coded bitplane */
	}
	else
	{
		if (bit_stat)
			bit_stat[0] = bits_written;
	}

	return bits_written;
}


/** \internal Decodes a bitplane of a block of coefficients using adaptive Rice
 * coding. Rice coding is a variant of Golomb coding. For more information see
 * \ref coding.
 * @param[in,out] dst Receives another bitplane of decoded coefficients. Needs
 *   space for at least num entries. Has to contain previous bit-planes.
 * @param[in] num number of coefficients.
 * @param[in] k initial value of \em k for the adaptive Rice coding. Has to be
 *   consistent between encoding and decoding!
 * @param[in] cur_bp Which bitplane we are currently decoding (only needed for
 *   figuring out where to store the sign bit).
 * @param[in] rlr_bits_left Number of run-length coded bits left from the
 *   higher bitplanes (aka non-refinement bits). Initially (for the top-most
 *   plane) this is num.
 * @param[in] bf file to read coded data from.
 * @return Number of run-length coded bits left from the higher bitplanes (aka
 *   non-refinement bits).
**/
int rice_decode_plane(wv_pel* dst, const int num, int k, int cur_bp, int rlr_bits_left, t_bit_file* bf)
{
	if (num > 0 && dst && bf)
	{
		wv_pel sign_flag = 0;
		int num_zeros = 0;
		int	idx = 0;
		int cur_rlr_bits_left = rlr_bits_left; /* how many coded bits are left in this bitplane */

		k = MAX(RICE_MIN_K, MIN(RICE_MAX_K, k)); /* make sure k is in valid range */

		/* arranged so that after all the bitplanes are read,
		  this flag ends up in the sign-bit position (MSB) */
		sign_flag = (wv_pel)1 << ((8 * sizeof (wv_pel)) - (1 + cur_bp));
		while (cur_rlr_bits_left > 0)
		{
			if (bit_read_single(bf) == 0)
			{ /* more zeros */
				num_zeros += 1 << k;
				cur_rlr_bits_left -= 1 << k;
				k = MIN(RICE_MAX_K, k + 1);
			}
			else
			{ /* a one, which means work */
				int added_zeros;
				unsigned char encoded_sign;

				/* read sign bit as well and increase # of 0s by 1 (2 as it's shifted >> 1),
				  the extra zero will become the new '1' bit */
				added_zeros = bit_read(k + 1, bf) + 2;
				k = MAX(RICE_MIN_K, k - 1);
				encoded_sign = added_zeros & 1; /* remember the sign */
				added_zeros = MIN(added_zeros >> 1, cur_rlr_bits_left); /* safety */
				cur_rlr_bits_left -= added_zeros;
				rlr_bits_left--; /* one less "significant" bit */
				num_zeros += added_zeros;

				for (; num_zeros > 0; idx++)
				{ /* store the accumulated 0 bits and at the same time read
				  in the refinement bits that occured during this run of 0s */
					if (dst[idx] != 0)
						dst[idx] = (dst[idx] << 1) | bit_read_single(bf); /* refinement */
					else
						num_zeros--; /* 0 << 1 | 0 is still 0 :D */
				}
				/* as we were looking for an extra zero which ended the loop */
				dst[idx - 1] = (sign_flag * encoded_sign) | 1;
			}
		}
		/* and read the last refinement bits (no need to write zeros ;)) */
		for (; idx < num; idx++)
			if (dst[idx] != 0)
				dst[idx] = (dst[idx] << 1) | bit_read_single(bf);

		return rlr_bits_left;
	}
	return 0;
}


/** \internal Finalizes a block with decoded bit-planes, this includes
 *   dequantization and properly setting the sign bit.
 * @param[in,out] dst Decoded bitplanes.
 * @param[in] num number of coefficients.
 * @param[in] last_bp The last bit-plane that was decoded, which lets us know
 *   by how much to dequantize the data and where the sign-bit ended up.
**/
void rice_decode_finalize(wv_pel* dst, const int num, const int last_bp)
{
	if (num > 0 && dst)
	{
		wv_pel sign_flag;
		int i;

		/* arranged so that after all the bitplanes are read,
		  this flag ends up in the sign-bit position (MSB) */
		sign_flag = (wv_pel)1 << ((8 * sizeof (wv_pel)) - (1 + last_bp));
		sign_flag--; /* set all the bits up to the sign bit */
		if (last_bp > 0)
		{ /* have to dequantize as well as set the sign properly */
			for (i = 0; i < num; i++)
			{
				wv_pel val = dst[i] & sign_flag; /* remove sign bit */

				val = (val > 0) ? (((val << 1) + 1) << last_bp) >> 1 : 0; /* dequant */
				if (dst[i] > sign_flag)
					dst[i] = -val; /* properly set sign (i.e. 2s complement) */
				else
					dst[i] = val;
			}
		}
		else
		{ /* only need to "properly" set the sign (we have only set the actual
		  sign bit, the other bits still represent a +ve number) */
			for (i = 0; i < num; i++)
				if (dst[i] < 0)
					dst[i] = -(dst[i] & sign_flag); /* properly set sign */
		}
	}
}
