/* utility.c, version 3.4.0

  Copyright (C) 2001-2007 Daniel Vollmer

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Vollmer <maven@maven.de>

*/

#include <math.h>
#include "utility.h"

/** Integer logarithm base 2 of a given integer.
 * This is equal to the number of bits needed to encode that number.
 * \warning Returns 0 for negative numbers!
 * @param[in] v number whose log is wanted
 * @return Integer logarithm (base 2) of v (wv_log2i(0) = 0)
**/
int wv_log2i(int v)
{
	int i;

	for (i = 0; v > 0; v >>= 1, i++) ;
	return i;
}

/** Converts MSE (mean square error) to PSNR (peak signal to noise ration (dB)).
 * Inverse of wv_psnr_to_mse().
 * @param[in] mse Mean square error
 * @return PSNR (in dB) of given MSE. The result is 0 (instead of "infinity")
 *   for non-positive values.
**/
double wv_mse_to_psnr(const double mse)
{
	if (mse > 0.0)
		return 20.0 * log10(255.0 / sqrt(mse));
	return 0.0;
}


/** Converts PSNR (peak signal to noise ration (dB)) to MSE (mean square error).
 * Inverse of wv_mse_to_psnr().
 * @param[in] psnr Peak signal to noise ration (in dB)
 * @return Mean square error of given PSNR. The result is 1e64 for non-positive
 *   values
**/
double wv_psnr_to_mse(const double psnr)
{
	if (psnr > 0.0)
		return pow(255.0 / pow(10.0, psnr / 20.0), 2.0);
	return 1e64;
}


/** Computes the PSNR (peak signal to noise ratio (dB)) and optionally the
 * mean-square error from two version of an image.
 * @param[in] a Image A
 * @param[in] b Image B
 * @param[in] width Width of the image in a and b
 * @param[in] height Height of the image in a and b
 * @param[in] pitch Pitch in wv_pel units of the image in a and b (usually this
 *   is a power of 2)
 * @param[out] pmse Address of a double where the mean-square error is written
 *   into (if != NULL)
 * @return PSNR between the two versions of the image
**/
double wv_calc_psnr(const wv_pel *a, const wv_pel *b, const int width, const int height, const int pitch, double *pmse)
{
	double sum2 = 0.0;
	int i, j;

	for (i = 0; i < height; i++)
		for (j = 0; j < width; j++)
		{
			double diff = (double)(a[i * pitch + j] - b[i * pitch + j]);

			sum2 += diff * diff;
		}

	if (sum2 > 0.0)
	{
		double mse = (double)(sum2 / (width * height)); // mean squared error

		if (pmse)
			*pmse = mse;
		return wv_mse_to_psnr(mse);
	}
	else
		*pmse = 0.0;
	return 0.0;
}


/* colour-space conversion from "YCoCg-R: A Color Space with RGB Reversibility
 * and Low Dynamic Range" by Henrique Malvar and Gary Sullivan. We implement
 * both the "normal" and the reversible variant. */


/** Converts an array of pixels (given as seperate bitplanes) from the RGB
 * color-space into the YCoCg color-space. Inverse of wv_ycocg_to_rgb().
 * @param[in] Num Number of pixels to be converted
 * @param[in,out] R Red pixels on input, Y pixels on output
 * @param[in,out] G Green pixels on input, Co pixels on output
 * @param[in,out] B Blue pixels on input, Cg pixels on output
 * @return Number of pixels converted
**/
int wv_rgb_to_ycocg(const int Num, wv_pel* R, wv_pel* G, wv_pel* B)
{
	if (Num > 0 && R && G && B)
	{
		int i;

		for (i = 0; i < Num; i++)
		{
			wv_pel rr = R[i], gg = G[i], bb = B[i];

			R[i] = (wv_pel)((rr + (gg << 1) + bb + 2) >> 2); // Y
			G[i] = (wv_pel)((rr - bb + 1) >> 1); // Co
			B[i] = (wv_pel)((-rr + (gg << 1) - bb + 2) >> 2); // Cg
		}
		return Num;
	}
	return 0;
}


/** Converts an array of pixels (given as seperate bitplanes) from the YCoCg
 * color-space into the RGB color-space. Inverse of wv_rgb_to_ycocg().
 * @param[in] Num Number of pixels to be converted
 * @param[in,out] Y Y pixels on input, red pixels on output
 * @param[in,out] Co Co pixels on input, green pixels on output
 * @param[in,out] Cg Cg pixels on input, blue pixels on output
 * @return Number of pixels converted
**/
int wv_ycocg_to_rgb(const int Num, wv_pel* Y, wv_pel* Co, wv_pel* Cg)
{
	if (Num > 0 && Y && Co && Cg)
	{
		int i;

		for (i = 0; i < Num; i++)
		{
			wv_pel yy = Y[i], co = Co[i], cg = Cg[i];
			wv_pel tmp;

			Co[i] = yy + cg;
			tmp = yy - cg;
			Y[i] = tmp + co;
			Cg[i] = tmp - co;
		}
		return Num;
	}
	return 0;
}


/** Converts an array of pixels (given as seperate bitplanes) from the RGB
 * color-space into the YCoCg-R color-space. Inverse of wv_ycocgr_to_rgb(). This
 * transform is reversible (i.e. lossless).
 * @param[in] Num Number of pixels to be converted
 * @param[in,out] R Red pixels on input, Y pixels on output
 * @param[in,out] G Green pixels on input, Co pixels on output
 * @param[in,out] B Blue pixels on input, Cg pixels on output
 * @return Number of pixels converted
**/
int wv_rgb_to_ycocgr(const int Num, wv_pel* R, wv_pel* G, wv_pel* B)
{
	if (Num > 0 && R && G && B)
	{
		int i;

		for (i = 0; i < Num; i++)
		{
			wv_pel rr = R[i], gg = G[i], bb = B[i];
			wv_pel tmp;

			G[i] = (wv_pel)(rr - bb); // Co
			tmp = bb + (G[i] >> 1);
			B[i] = (wv_pel)(gg - tmp); // Cg
			R[i] = (wv_pel)(tmp + (B[i] >> 1)); // Y

		}
		return Num;
	}
	return 0;
}


/** Converts an array of pixels (given as seperate bitplanes) from the YCoCg-R
 * color-space into the RGB color-space. Inverse of wv_rgb_to_ycocgr(). This
 * transform is reversible (i.e. lossless).
 * @param[in] Num Number of pixels to be converted
 * @param[in,out] Y Y pixels on input, red pixels on output
 * @param[in,out] Co Co pixels on input, green pixels on output
 * @param[in,out] Cg Cg pixels on input, blue pixels on output
 * @return Number of pixels converted
**/
int wv_ycocgr_to_rgb(const int Num, wv_pel* Y, wv_pel* Co, wv_pel* Cg)
{
	if (Num > 0 && Y && Co && Cg)
	{
		int i;

		for (i = 0; i < Num; i++)
		{
			wv_pel yy = Y[i], co = Co[i], cg = Cg[i];
			wv_pel tmp;

			tmp = yy - (cg >> 1);
			Co[i] = cg + tmp;
			Cg[i] = tmp - (co >> 1);
			Y[i] = Cg[i] + co;
		}
		return Num;
	}
	return 0;
}
