/** \file config.h Compile time configuration options. **/
/* version 3.4.0

  Copyright (C) 2001-2007 Daniel Vollmer

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Vollmer <maven@maven.de>

*/

#ifndef _CONFIG_H_
#define _CONFIG_H_

#ifdef __cplusplus // C++
extern "C" {
#endif

/** This is the basic (signed) "pixel" type.
 * Minimum size for 8 bit input channels is ~12 bits (so usually at least 16
 * are used).
 * \Warning Can have at most 64bits (due to wv_PEL_STORE_BITS)!
**/
typedef short int wv_pel;

/** Signed integer type used to represent error improvements.
 * Should be at least 32 bits, and larger than that when compressing more than
 * 8bpp data; for decompression 32 bits should always be enough.
 */
typedef int wv_fixed_point;

/** Indirectly defines the # of channels that can be stored in a file.
 * This limit is fairly arbitrary, although some arrays depending on this size
 * are created on the stack.
 * \Warning Changing it will make the bit-stream format incompatible.
**/
#define wv_MAX_CHANNEL_STORE 4
/** Maximum amount of channels that can be stored in a compressed file. **/
#define wv_MAX_CHANNELS (1 << wv_MAX_CHANNEL_STORE)

/** Define to show assertions (useful during development) **/
/*#define wv_ENABLE_ASSERTS*/


#ifdef wv_ENABLE_ASSERTS
#include <assert.h> /* so we don't have to include at the top of each file */
#define wv_ASSERT(c,r) assert((c))
#else
#define wv_ASSERT(c,r) ((void)0)
#endif

#ifdef __cplusplus // C++
}
#endif

#endif /* _CONFIG_H_ */
