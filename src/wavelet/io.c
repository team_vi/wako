/* io.c, version 3.4.0

  Copyright (C) 2001-2007 Daniel Vollmer

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Vollmer <maven@maven.de>

*/

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "io.h"
#include "coding.h"
#include "utility.h"

/** Current header-version. **/
#define wv_CUR_HEADER_VERSION 4
/** Current bitstream-version. **/
#define wv_CUR_BITSTREAM_VERSION 5

const char wv_copyright[] = " wavelet 3.5.0 Copyright 2001-2017 Daniel Vollmer, Gilson Varghese";
static const unsigned int wv_magic = 0x5b765d00 | (wv_CUR_HEADER_VERSION << 4) | wv_CUR_BITSTREAM_VERSION;

/** \Internal In how many bits we store the required size (in bytes) of wv_pel (1 << n).
 * 2 bits allow for a max value of 3, which allows for up to 64bit wv_pels.
**/
#define wv_PEL_STORE_BITS 2

/** Callback function for the scheduler. **/
typedef void (*t_scheduler_callback)(int ci, const t_wv_channel *cc, const t_wv_schedule_entry *se, double mse, int bits_allocated, void* userdata);

/** \internal Interleaves a number of schedules so that the currently lowest-
 * fidelity channel is written next. This takes a channel's given target mse
 * as well as their requested relative final errors into account. The process
 * is stopped once all constraints are satisfied or we have no more bits in
 * our budget.
 * @param[in] NumChannels # of channels in Channel.
 * @param[in] Channel NumChannels pointers to t_wv_channel, which actually
 *   treated as const (so they are used read-only). Not declared as const due
 *    http://c-faq.com/ansi/constmismatch.html. Need identical dimensions.
 * @param[in] TargetMSE NumChannels doubles that prescribe the target mean-
 *   square for each given channel. If the value is non-negative then it is
 *   an absolute value which is strictly required (i.e. the method will return
 *   with an error-code if it cannot be achieved), if it is negative then it
 *   is a relative value that is resolved (if possible) with its closest
 *   absolute neighbour, first checked on the right, then on the left side.
 * @param[in] MaxBits Bits allocated to compressed data, 0 ^= infinite bits.
 * @param[in] MinBits Minimum number of bits spent on each channel. These is
 *   interpreted as a hard constraint, and failure to satisfy them results in
 *   failure, unless the channel had fewer bits to start with. This number
 *   will be rounded up depending on the granularity of the blocks.
 * @param[in] cb Callback function to be executed for each bitplane of each
 *   block.
 * @param[in] userdata Userdata passed onto the cb callback-function.
 * @return Number of bits written (or -1 on failure to satisfy constraints).
**/
static int interleave_schedules(const int NumChannels, t_wv_channel* Channel[], const double TargetMSE[], int MaxBits, const int MinBits[], t_scheduler_callback cb, void* userdata)
{
	double fp_factor;
	wv_fixed_point happy_mse[wv_MAX_CHANNELS + 1]; /* for achieving happiness */
	wv_fixed_point terminate_mse[wv_MAX_CHANNELS + 1]; /* for terminating the loop */
	wv_fixed_point ofs_mse[wv_MAX_CHANNELS + 1];
	wv_fixed_point cur_mse[wv_MAX_CHANNELS + 1];
	wv_fixed_point ofs_min, ofs_max;
	int channel_bits = wv_log2i(NumChannels - 1);
	int i, j, entries_left, happy_count, initial_bits, left_bits;
	int next_entry[wv_MAX_CHANNELS + 1];
	int min_bits[wv_MAX_CHANNELS + 1], min_bits_needed;
	unsigned short next_plane[wv_MAX_CHANNELS + 1][wv_MAX_BLOCKS + 1];
	unsigned short right_shift[wv_MAX_CHANNELS + 1];
	unsigned short am_i_happy[wv_MAX_CHANNELS + 1];
	unsigned short min_frac;

	/* find the smalllest shift value, as we'll use that for all channels */
	min_frac = 1023;
	for (i = 0; i < NumChannels; i++)
		min_frac = MIN(min_frac, Channel[i]->schedule_fractional_bits);
	fp_factor = pow(2.0, min_frac);

	/* initialise everything */
	entries_left = 0;
	ofs_max = -1;
	ofs_min = 0;
	min_bits_needed = 0;
	for (i = 0; i < NumChannels; i++)
	{
		double target;

		next_entry[i] = 0;
		for (j = 0; j < Channel[i]->num_blocks; j++)
			next_plane[i][j] = 0;

		entries_left += Channel[i]->num_schedule_entries;
		right_shift[i] = Channel[i]->schedule_fractional_bits - min_frac;

		cur_mse[i] = ofs_mse[i] = 0;
		min_bits[i] = 0;
		for (j = 0; j < Channel[i]->num_schedule_entries; j++)
		{
			const t_wv_schedule_entry *se = &Channel[i]->schedule[j];

			cur_mse[i] += ((se->improvement - 1) >> right_shift[i]) + 1;
			if (min_bits[i] < MinBits[i])
			{
				min_bits[i] += se->num_bits;
				min_bits_needed += channel_bits * se->count; /* need to include overhead in total */
			}
		}
		min_bits_needed += min_bits[i];

		/* limit to within [-cur_mse, cur_mse] for each channel to prevent overflows */
		target = TargetMSE[i] * fp_factor;
		if (target >= 0.0)
			terminate_mse[i] = happy_mse[i] = MIN(target, cur_mse[i]);
		else
		{
			ofs_mse[i] = MAX(target, -cur_mse[i]);
			if (ofs_mse[i] == 0)
				ofs_mse[i] = -1; /* make sure we didn't round a negative target mse to 0 */
		}

		ofs_max = MAX(ofs_max, ofs_mse[i]);
		ofs_min = MIN(ofs_min, ofs_mse[i]);
	}

	if (MaxBits > 0 && MaxBits < min_bits_needed)
		return -1; /* cannot satisfy MinBits criterion */

	if (ofs_min < 0)
	{ /* we have relative values */
		if (ofs_max >= 0)
		{ /* and we can fix them to absolute values */
			for (i = 0; i < NumChannels; i++)
			{
				if (ofs_mse[i] < 0)
				{
					for (j = 1; j <= MAX(i, NumChannels - i - 1); j++)
					{
						int ref = -1;

						if (i + j < NumChannels && ofs_mse[i + j] >= 0)
							ref = i + j; /* first look on the right */
						else if (i - j >= 0 && ofs_mse[i - j] >= 0)
							ref = i - j; /* next look on the left */
						if (ref >= 0)
						{
							happy_mse[i] = MAX(0, happy_mse[ref] + ofs_mse[i]);
							terminate_mse[i] = MAX(0, terminate_mse[ref] + ofs_mse[i]);
							ofs_mse[i] += ofs_mse[ref];
							break;
						}
					}
				}
			}
		}
		else
		{ /* everything is relative */
			for (i = 0; i < NumChannels; i++)
			{
				happy_mse[i] = cur_mse[i];
				terminate_mse[i] = 0; /* and ofs stays as it is */
			}
		}
	}

	/* first go through all the channels to see whether they are already happy */
	happy_count = 0;
	for (i = 0; i < NumChannels; i++)
	{
		am_i_happy[i] = cur_mse[i] <= terminate_mse[i];
		happy_count += am_i_happy[i];
//		printf("%i. terminate_mse: %i (%f) happy_mse: %i (%f) ofs_mse: %i (%f)\n", i, terminate_mse[i], terminate_mse[i] / fp_factor, happy_mse[i], happy_mse[i] / fp_factor, ofs_mse[i], ofs_mse[i] / fp_factor);
	}

	left_bits = initial_bits = (MaxBits > 0) ? MaxBits : 1 << 30;
	while (happy_count < NumChannels && left_bits > channel_bits && entries_left > 0)
	{
		wv_fixed_point worst_mse = (wv_fixed_point)-1 << (sizeof worst_mse * 8 - 1);
		int wi = -1; /* worst index */

		/* find the currently worst channel */
		for (i = 0; i < NumChannels; i++)
			if (!am_i_happy[i] && cur_mse[i] - ofs_mse[i] >= worst_mse && next_entry[i] < Channel[i]->num_schedule_entries && (min_bits_needed == 0 || min_bits[i] > 0 || Channel[i]->schedule[next_entry[i]].count * channel_bits + Channel[i]->schedule[next_entry[i]].num_bits + min_bits_needed <= left_bits))
			{ /* make sure we can still satisfy min_bits_needed if this one doesn't reduce it */
				worst_mse = cur_mse[i] - ofs_mse[i];
				wi = i;
			}

		if (wi >= 0)
		{
			wv_fixed_point improvement;
			const t_wv_schedule_entry *se = &Channel[wi]->schedule[next_entry[wi]];
			int entry_overhead = channel_bits * se->count; /* that many channel indices */
			int bits_allocated;

			improvement = ((se->improvement - 1) >> right_shift[wi]) + 1; /* convert to common scale */

			if (left_bits >= se->num_bits + entry_overhead)
			{ /* we can fit the next block */
				bits_allocated = se->num_bits + entry_overhead;
				left_bits -= bits_allocated;
				if (min_bits[wi] > 0)
				{ /* we made sure min_bits always contain whole blocks, so we always have enough space for the whole block */
					min_bits[wi] -= se->num_bits;
					min_bits_needed -= bits_allocated;
				}
			}
			else
			{ /* we write a partial block (and there must be bits left) */
				improvement = ((improvement / fp_factor) * ((double)(left_bits - entry_overhead) / se->num_bits)) * fp_factor;
				bits_allocated = left_bits;
				left_bits = 0;
			}
			cur_mse[wi] -= improvement;

			if (cb)
				cb(wi, Channel[wi], se, cur_mse[wi] / fp_factor, bits_allocated, userdata);

			next_entry[wi]++;
			entries_left--;

			/* update whether this channel it is happy now */
			am_i_happy[wi] = cur_mse[wi] <= terminate_mse[wi];
			happy_count += am_i_happy[wi];
		}
	}

	for (i = 0; i < NumChannels; i++)
		if (!am_i_happy[i] && cur_mse[i] <= happy_mse[i])
			happy_count++; /* we would have been happy no matter what */

	wv_ASSERT(min_bits_needed == 0, "Could not satisfy MinBits criterion");
	return (happy_count == NumChannels) ? initial_bits - left_bits : -1;
}


typedef struct
{
	double mse[wv_MAX_CHANNELS + 1];
	int bits[wv_MAX_CHANNELS + 1];
	t_bit_file* bf;
	unsigned int *refinement_cache; /* needs to provide space for the (biggest block >> 5) + 1 entries */
	unsigned char channel_bits;
	unsigned char block_bits;
} t_write_userdata;

static void scheduler_write_callback(int ci, const t_wv_channel *cc, const t_wv_schedule_entry* se, double mse, int bits_allocated, void* userdata)
{
	t_write_userdata *ud = (t_write_userdata *)userdata;

	ud->mse[ci] = mse; /* store mse */
	ud->bits[ci] += bits_allocated;
	if (ud->bf)
	{
		const t_wv_block *bl = &cc->block[se->b];
		unsigned char j;

		for (j = 0; j < se->count; j++)
		{ /* go through agglomerated bit-planes */
			if (ud->channel_bits > 0)
				bit_write(ci, ud->channel_bits, ud->bf);
			if (ud->block_bits > 0)
				bit_write(se->b, ud->block_bits, ud->bf);
			if (se->b == 0)
				bit_write(1, 1, ud->bf); /* indicate we're still writing */
			rice_encode_plane(cc->reordered_channel + bl->offset, bl->size, 0,
				bl->num_planes - (se->p + j) - 1, ud->refinement_cache, ud->bf);
		}
	}
//	printf("channel: %i block: %i plane: %i count: %i size: %i mse: %f\n", ci, se->b, se->p, se->count, se->num_bits, mse);
}


/** Returns the size of the header in bits (for the given channels).
 * @param[in] NumChannels # of channels in Channel.
 * @param[in] Channel NumChannels pointers to t_wv_channel, which actually
 *   treated as const (so they are used read-only). Not declared as const due
 *    http://c-faq.com/ansi/constmismatch.html. Need identical dimensions.
 * @return Number of bits used for the header (or -1 on failure).
**/
int wv_query_header(const int NumChannels, t_wv_channel* Channel[])
{
	int bits = -1;

	if (NumChannels > 0 && NumChannels <= wv_MAX_CHANNELS && Channel)
	{
		int i;
		unsigned char min_pel_store;

		/* check that the channels are the same size & # of blocks */
		min_pel_store = Channel[0]->min_pel_store;
		for (i = 1; i < NumChannels; i++)
		{
			min_pel_store = MAX(min_pel_store, Channel[i]->min_pel_store);
			if (Channel[0]->width != Channel[i]->width || Channel[0]->height != Channel[i]->height)
				return -1;
		}

		bits = 32 + 16 + 16 + wv_MAX_CHANNEL_STORE + wv_MAX_BLOCK_STORE + wv_PEL_STORE_BITS; /* magic + width + height + channels + blocks + pel */
		bits += NumChannels * (8 + Channel[0]->num_blocks * (min_pel_store + 3));
	}
	return bits;
}


/** Queries the scheduler what sort of mean-square error would be achieved.
 * @param[in] NumChannels # of channels in Channel.
 * @param[in] Channel NumChannels pointers to t_wv_channel, which actually
 *   treated as const (so they are used read-only). Not declared as const due
 *    http://c-faq.com/ansi/constmismatch.html. Need identical dimensions.
 * @param[in] TargetMSE NumChannels doubles that prescribe the target mean-
 *   square for each given channel. If the value is non-negative then it is
 *   an absolute value which is strictly required (i.e. the method will return
 *   with an error-code if it cannot be achieved), if it is negative then it
 *   is a relative value that is resolved (if possible) with its closest
 *   absolute neighbour, first checked on the right, then on the left side.
 * @param[in] MaxBits Bits allocated to compressed data, 0 ^= infinite bits.
 * @param[in] MinBits Minimum number of bits spent on each channel. These is
 *   interpreted as a hard constraint, and failure to satisfy them results in
 *   failure, unless the channel had fewer bits to start with. This number
 *   will be rounded up depending on the granularity of the blocks.
 * @param[out] EstimatedMSE Estimated Mean-square error achieved for each channel.
 * @param[out] EstimatedBits Estimated bits used for each channel.
 * @return Number of bits used (or -1 on failure to satisfy constraints).
**/
int wv_query_scheduler(const int NumChannels, t_wv_channel* Channel[], const double TargetMSE[], const int MaxBits, const int MinBits[], double EstimatedMSE[], int EstimatedBits[])
{
	int bits = -1;
	int i, headersize, bits_available = MaxBits;
	t_write_userdata ud;

	/* verify parameters */
	if (NumChannels <= 0 || !Channel)
		return -1;

	headersize = wv_query_header(NumChannels, Channel);
	if (headersize <= 0)
		return headersize;

	if (bits_available > 0)
	{
		bits_available -= headersize;
		if (bits_available <= 0)
			return -1; /* couldn't even write header */
	}

	/* make an (over-estimated) guess about the initial error of each channel */
	for (i = 0; i < NumChannels; i++)
	{
		ud.mse[i] = pow(2.0, sizeof (wv_fixed_point) * 8 - 2 - Channel[i]->schedule_fractional_bits) - 1.0;
		ud.bits[i] = 0;
	}

	ud.bf = NULL;
	ud.refinement_cache = NULL;
	ud.channel_bits = wv_log2i(NumChannels - 1);
	ud.block_bits = wv_log2i(Channel[0]->num_blocks - 1);
	bits = interleave_schedules(NumChannels, Channel, TargetMSE, bits_available, MinBits, scheduler_write_callback, &ud);
	for (i = 0; i < NumChannels; i++)
	{
		EstimatedMSE[i] = ud.mse[i];
		EstimatedBits[i] = ud.bits[i];
	}
	if (bits > 0)
		bits += headersize;
	return bits;
}


/** Encodes a number of channels progressively interleaved into a given file
 * which can later be read by wv_decode(). As each channel has to have the same
 * dimensions, they also have the same amount of blocks. The blocks are ordered
 * by the scheduler to always give the best quality improvement per bit spent.
 * @param[in] NumChannels # of channels in Channel.
 * @param[in] Channel NumChannels pointers to t_wv_channel, which actually
 *   treated as const (so they are used read-only). Not declared as const due
 *    http://c-faq.com/ansi/constmismatch.html. Need identical dimensions.
 * @param[in] TargetMSE NumChannels doubles that prescribe the target mean-
 *   square for each given channel. If the value is non-negative then it is
 *   an absolute value which is strictly required (i.e. the method will return
 *   with an error-code if it cannot be achieved), if it is negative then it
 *   is a relative value that is resolved (if possible) with its closest
 *   absolute neighbour, first checked on the right, then on the left side.
 * @param[in] MaxBits Bits allocated for data (including the header!),
 *   0 ^= infinite bits.
 * @param[in] MinBits Minimum number of bits spent on each channel. These is
 *   interpreted as a hard constraint, and failure to satisfy them results in
 *   failure, unless the channel had fewer bits to start with. This number
 *   will be rounded up depending on the granularity of the blocks.
 * @param[in] BF Bit-file handle to be written to, must have been opened with
 *   the correct number of bits (otherwise the file will be too big).
 * @return Number of bits written (or -1 on failure to satisfy constraints).
**/
int wv_encode(const int NumChannels, t_wv_channel* Channel[], const double TargetMSE[], const int MaxBits, const int MinBits[], t_bit_file* BF)
{
	int bits = -1;

	if (NumChannels > 0 && NumChannels <= wv_MAX_CHANNELS && Channel && BF)
	{
		int i, j;
		t_write_userdata ud;
		unsigned char min_pel_store;

		/* check that the channels are the same size & # of blocks */
		min_pel_store = Channel[0]->min_pel_store;
		for (i = 1; i < NumChannels; i++)
		{
			min_pel_store = MAX(min_pel_store, Channel[i]->min_pel_store);
			if (Channel[0]->width != Channel[i]->width || Channel[0]->height != Channel[i]->height)
				return -1;
		}

		bits = 0;
		bits += bit_write(wv_magic, 32, BF);
		bits += bit_write(Channel[0]->width - 1, 16, BF);
		bits += bit_write(Channel[0]->height - 1, 16, BF);
		bits += bit_write(NumChannels - 1, wv_MAX_CHANNEL_STORE, BF);
		bits += bit_write((Channel[0]->num_blocks - 1) / 3, wv_MAX_BLOCK_STORE, BF);
		bits += bit_write(min_pel_store, wv_PEL_STORE_BITS, BF);

		/* write retained user-data for all channels as well as # of bitplanes */
		for (j = 0; j < NumChannels; j++)
		{
			bits += bit_write(Channel[j]->data_format, 8, BF);
			for (i = 0; i < Channel[0]->num_blocks; i++)
				bits += bit_write(Channel[j]->block[i].num_planes, min_pel_store + 3, BF);
		}

		if (MaxBits > 0 && MaxBits - bits <= 0)
			return -1; /* not enough space for data */

		ud.bf = BF;
		/* allocate refinement cache that is big enough for the largest block */
		ud.refinement_cache = malloc(((Channel[0]->block[Channel[0]->num_blocks - 1].size >> 5) + 1) * sizeof *ud.refinement_cache);
		ud.channel_bits = wv_log2i(NumChannels - 1);
		ud.block_bits = wv_log2i(Channel[0]->num_blocks - 1);
		j = interleave_schedules(NumChannels, Channel, TargetMSE, MaxBits - bits, MinBits, scheduler_write_callback, &ud);
		if (j < 0)
			bits = j; /* gotten an error */
		else
			bits += j;
		free(ud.refinement_cache);
	}
	return bits;
}


/** Reads the header written by wv_encode().
 * @param[in] Header Store the header information here (only valid if non-NULL
 *   was returned).
 * @param[in] BF Bit-file handle to try to read header from.
 * @return Header on success or NULL on failure to find a valid header.
**/
t_wv_header* wv_read_header(t_wv_header* Header, t_bit_file* BF)
{
	if (!Header)
		return NULL;

	memset(Header, 0, sizeof *Header);

	if (!BF)
		return NULL;

	if (bit_read(32, BF) != wv_magic)
		return NULL;

	Header->width = 1 + bit_read(16, BF);
	Header->height = 1 + bit_read(16, BF);
	Header->num_channels = 1 + bit_read(wv_MAX_CHANNEL_STORE, BF);
	Header->num_blocks = 1 + 3 * bit_read(wv_MAX_BLOCK_STORE, BF);
	Header->min_pel_store = bit_read(wv_PEL_STORE_BITS, BF);
	wv_ASSERT(sizeof(wv_pel) >= 1U << Header->min_pel_store, "Too many bitplanes in file: Increase range of wv_pel");

	return Header;
}


/** Decodes channels written by wv_encode() to a file back to a t_wv_channel.
 * The channels returned by this function can be written again by wv_encode(),
 * but the bit-estimate will be incorrect and actual output will differ. Also,
 * no estimated errors are available. Thus using wv_channel_set_max_mse() or
 * the relative equivalent on these channels is not recommended.
 * @param[in] Header If the header was already read manually, a pointer to it,
 *   if NULL, the header is read from the file BF.
 * @param[in] Reduction Indicates how often to halve the dimensions of the
 *   stored channels (until the smaller of width and height is 2). Although
 *   this is a very natural thing to do with wavelet coding, the completely
 *   embedded structure of the files does not allow this to be of the highest
 *   quality in the file as we have to stop as soon as the first bitplane of a
 *   higher-resolution block is encountered.
 * @param[out] Channel Array receiving the returned number of t_wv_channel*
 *   pointers.
 * @param[in] BF Bit-file handle to read from. If Header != NULL, it has to be
 *   aligned directly after the header, otherwise on the header.
 * @return Number of channels decoded, 0 on failure.
**/
int wv_decode(const t_wv_header* Header, int Reduction, t_wv_channel* Channel[wv_MAX_CHANNELS + 1], t_bit_file* BF)
{
	t_wv_header hdr;

	if (!Header)
		Header = wv_read_header(&hdr, BF); /* not given, so try to read it */

	if (Channel && Header)
	{ /* looks like we're good */
		t_wv_channel *cc = NULL;
		t_wv_schedule_entry* last_block[wv_MAX_CHANNELS + 1]; /* the last time we wrote a block of that channel */
		int width2 = 1 << wv_log2i(Header->width - 1); /* next bigger pow2 */
		int height2 = 1 << wv_log2i(Header->height - 1); /* next bigger pow2 */
		int num_levels = MAX(0, wv_log2i(MIN(width2, height2)) - 2);
		num_levels = 3;
		int size, total_planes;
		int i, j;
		int rlr_bits_left[wv_MAX_CHANNELS + 1][wv_MAX_BLOCKS + 1];
		unsigned char planes_left[wv_MAX_CHANNELS + 1][wv_MAX_BLOCKS + 1];
		unsigned char block_bits, channel_bits;

		if (num_levels != (Header->num_blocks - 1) / 3)
			return 0;

		Reduction = MAX(0, MIN(Reduction, num_levels));
		total_planes = 0;
		for (j = 0; j < Header->num_channels; j++)
		{ /* initialize all the channels to something sensible */
			Channel[j] = malloc(sizeof *Channel[j]);
			cc = Channel[j];
			cc->width = (Header->width + (1 << Reduction) - 1) >> Reduction;
			cc->height = (Header->height + (1 << Reduction) - 1) >> Reduction;
			cc->num_schedule_entries = 0;
			cc->num_blocks = Header->num_blocks - Reduction * 3;
			cc->schedule_fractional_bits = 0;
			cc->data_format = bit_read(8, BF);
			cc->min_pel_store = Header->min_pel_store;
			cc->block = malloc(cc->num_blocks * sizeof *cc->block);

			size = 0;
			for (i = 0; i < cc->num_blocks; i++)
			{
				int shift = Reduction + (cc->num_blocks - 1) / 3 - MAX(0, i - 1) / 3; /* shift is the same for i == 0 and 1-3 */
				int used_width, used_height;

				if (MIN(width2, height2) <= 2)
					shift = 0; /* only the untransformed data written as average */
				get_block_dimensions(width2, Header->width, height2, Header->height, shift, &used_width, &used_height);
				cc->block[i].offset = (i == 0) ? 0 : cc->block[i - 1].offset + cc->block[i - 1].size;
				cc->block[i].size = used_width * used_height;
				size += cc->block[i].size;
				rlr_bits_left[j][i] = cc->block[i].size; /* initially all coefficients are run-length coded */

				cc->block[i].num_planes = bit_read(Header->min_pel_store + 3, BF);
				cc->block[i].plane = NULL;
				planes_left[j][i] = cc->block[i].num_planes;
				cc->num_schedule_entries += cc->block[i].num_planes; /* not agglomerated */
			}
			bit_read((Header->min_pel_store + 3) * 3 * Reduction, BF); /* skip # bit-planes for blocks that were "reduced away" */

			total_planes += cc->num_schedule_entries;
			cc->schedule = malloc(cc->num_schedule_entries * sizeof *cc->schedule);
			cc->num_schedule_entries = 0; /* fill this out more accurately when reading */
			cc->reordered_channel = malloc(size * sizeof *cc->reordered_channel);
			memset(cc->reordered_channel, 0, size * sizeof *cc->reordered_channel);
			last_block[j] = NULL; /* for computing improvements */
		}

		num_levels -= Reduction;
		width2 >>= Reduction;
		height2 >>= Reduction;

		block_bits = wv_log2i(Header->num_blocks - 1); /* bits need for coding a block-# */
		channel_bits = wv_log2i(Header->num_channels - 1); /* bits need for coding a channel-# */

		while (total_planes > 0)
		{
			t_wv_schedule_entry *se;
			int nc;
			unsigned char nb;

			nc = bit_read(channel_bits, BF);
			nb = bit_read(block_bits, BF);
			/* stop if at least one of the indices is incorrent (either corrupted or absent data) */
			if (nc >= Header->num_channels || nb >= Channel[0]->num_blocks || planes_left[nc][nb] == 0)
				break;

			/* for block 0, we've written a single 1-bit to indicate valid data */
			if (nb == 0 && bit_read_single(BF) == 0)
				break;

			/* now we need a bitplane for nc's nb */
			cc = Channel[nc];

			/* first, fill out the schedule entry */
			se = cc->schedule + cc->num_schedule_entries;
			cc->num_schedule_entries++;
			se->num_bits = block_bits + 1; /* some value */
			se->improvement = total_planes;
			se->count = 1;
			se->b = nb;
			se->p = cc->block[nb].num_planes - planes_left[nc][nb]; /* as 0 ^= highest plane */
			if (last_block[nc])
				last_block[nc]->improvement -= total_planes; /* update the last block's improvements so that this block happens in the right place */
			last_block[nc] = se;

			planes_left[nc][nb]--;
			rlr_bits_left[nc][nb] = rice_decode_plane(cc->reordered_channel + cc->block[nb].offset,
				cc->block[nb].size, 0, planes_left[nc][nb], rlr_bits_left[nc][nb], BF);
			total_planes--;
		}

		/* now finalize the bitplanes (i.e. dequant and properly set sign) */
		for (j = 0; j < Header->num_channels; j++)
		{
			cc = Channel[j];
			for (i = 0; i < cc->num_blocks; i++)
				rice_decode_finalize(cc->reordered_channel + cc->block[i].offset,
					cc->block[i].size, planes_left[j][i]);
		}

		return Header->num_channels;
	}
	return 0;
}
