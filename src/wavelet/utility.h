/** \file utility.h Common utility functions. **/
/* version 3.4.0

  Copyright (C) 2001-2007 Daniel Vollmer

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Vollmer <maven@maven.de>

*/

#ifndef _UTILITY_H_
#define _UTILITY_H_

#ifdef __cplusplus // C++
extern "C" {
#endif

#include "config.h"

#ifndef MIN
#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#endif

int wv_log2i(int max_val);
double wv_mse_to_psnr(const double mse);
double wv_psnr_to_mse(const double psnr);
double wv_calc_psnr(const wv_pel *a, const wv_pel *b, const int width, const int height, const int pitch, double *pmse);

int wv_rgb_to_ycocg(const int Num, wv_pel* R, wv_pel* G, wv_pel* B);
int wv_ycocg_to_rgb(const int Num, wv_pel* Y, wv_pel* Cb, wv_pel* Cr);

int wv_rgb_to_ycocgr(const int Num, wv_pel* R, wv_pel* G, wv_pel* B);
int wv_ycocgr_to_rgb(const int Num, wv_pel* Y, wv_pel* Cb, wv_pel* Cr);

#ifdef __cplusplus // C++
}
#endif

#endif /* _UTILITY_H_ */
