/* channel.c, version 3.4.0

  Copyright (C) 2001-2007 Daniel Vollmer

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Vollmer <maven@maven.de>

*/

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "channel.h"
#include "coding.h"
#include "transform.h"
#include "utility.h"

#ifndef M_LOG2E
#define M_LOG2E 1.4426950408889634074 /* log_2 e */
#endif

/** \internal Agglomerate bitplanes so that we don't get high bps blocking
 * lower ones with a better improvement per bit.
 * Usually the improvement per bit of a higher bitplane is better than the
 * ones following it because higher planes contain more zeros (i.e. more
 * efficient run-length coding) and thus use less bits. In addition, they also
 * improve more than lower bitplanes (as lower bitplanes only impart a smaller
 * change). Unfortunately this is not always the case (most likely due to the
 * error estimation). So what we do is we agglomerate (combine) bitplanes
 * until the above is true.
**/
static int agglomerate_bitplanes(int num_planes, t_wv_bitplane* plane)
{
	int agg_count = 0;
	int current_plane = 0;
	int next_plane = 1;

	while (next_plane < num_planes)
	{
		if (plane[current_plane].improvement_per_bit < plane[next_plane].improvement_per_bit)
		{ /* this plane improve less per bit than the next (less significant!) one */
			/* add their improvement and number of bits and recompute improvement per bit */
			plane[current_plane].improvement += plane[next_plane].improvement;
			plane[current_plane].num_bits += plane[next_plane].num_bits;
			plane[current_plane].improvement_per_bit = plane[current_plane].improvement / plane[current_plane].num_bits;
			plane[current_plane].count++;
			/* and the next plane is gone */
			plane[next_plane].improvement = 0.0;
			plane[next_plane].count = 0;
			next_plane++;
			agg_count++;
		}
		else
			current_plane = next_plane++; /* look at the next 2 planes */
	}
	return agg_count;
}


/** \internal Fills a schedule from the entries of the bitplanes.
 * The schedule contains the order in which bitplanes from block are written.
 * The ordering criterium is the improvement per bit, under the constraint that
 * the bitplanes are written from the top down (i.e. most significant bits
 * first) and that no bit-planes can be skipped. This means we only have to
 * choose from which block the next bitplanes comes.
 * Also computes the amount of space for fractional bits in wv_fixed_point.
**/
static void convert_planes_to_schedule(t_wv_channel* cc)
{
	double multiplier;
	t_wv_schedule_entry *se;
	int i, j;
	int fractional_bits;
	unsigned char next_plane[wv_MAX_BLOCKS + 1];

	cc->num_schedule_entries = 0;
	multiplier = 1.0; /* (Ceiling[Log[2, -->1<-- + 2^Range[0, 127]]] == Range[1, 128]) -> True */
	for (i = 0; i < cc->num_blocks; i++)
	{
		next_plane[i] = 0; /* initialize next_plane, so we start at the top */
		for (j = 0; j < cc->block[i].num_planes; j++)
		{
			multiplier += MAX(0.0, cc->block[i].plane[j].improvement); /* agglomerated entries have improvement 0.0 */
			cc->num_schedule_entries += cc->block[i].plane[j].count > 0;
		}
	}
	/* leave 1 bit for sign and 1 bit for overflows as we force the minimum improvement to be 1 */
	fractional_bits = sizeof(wv_fixed_point) * 8 - 2 - ceil(log(multiplier)*M_LOG2E);
	wv_ASSERT(fractional_bits >= 2, "Not enough fractional precision bits / possible overflow: Increase size of wv_fixed_point");
	cc->schedule_fractional_bits = MAX(0, fractional_bits);
	multiplier = pow(2.0, cc->schedule_fractional_bits);

	cc->schedule = malloc(cc->num_schedule_entries * sizeof *cc->schedule);

	/* now add the bitplanes in the right order */
	for (i = 0, se = cc->schedule; i < cc->num_schedule_entries; i++, se++)
	{
		double best_improvement = -1e64;
		t_wv_bitplane *bp;
		unsigned char best_block = 0;

		/* find the next entry with the best improvement per size */
		for (j = 0; j < cc->num_blocks; j++)
			if (next_plane[j] < cc->block[j].num_planes && cc->block[j].plane[next_plane[j]].improvement_per_bit > best_improvement)
			{
				best_improvement = cc->block[j].plane[next_plane[j]].improvement_per_bit;
				best_block = j;
			}

		bp = &cc->block[best_block].plane[next_plane[best_block]];

		se->b = best_block;
		se->p = next_plane[best_block];
		se->num_bits = bp->num_bits;
		se->improvement = MAX(1, bp->improvement * multiplier);
		se->count = bp->count;
		next_plane[best_block] += bp->count; /* update next_plane */
//		printf("block: %i plane: %i imp: %f size: %i imp/b: %f (%i)\n", se->b, se->p, se->improvement / multiplier, se->num_bits, (se->improvement / multiplier) / se->num_bits, se->count);
	}
	/* now we can already free the planes (as all the information is in the schedule) */
	for (i = cc->num_blocks - 1; i >= 0; i--)
		if (cc->block[i].plane)
		{
			free(cc->block[i].plane);
			cc->block[i].plane = NULL;
		}
}


/** Initializes a channel for subsequent encoding.
 * (Lossless) Inverse of wv_channel_uncompact().
 * \Warning The actual width and height of the data in Data has to be the
 * next larger power of 2!
 * \Warning The data in Data is modified!
 * @param[in] Width Width of the \em used data in Data.
 * @param[in] Height Height of the \em used data in Data.
 * @param[in] DataFormat Format of the data stored, or any other 8-bit value
 *   you want to store in an encoded channel. This information can be queried
 *   with wv_channel_get_data_format().
 * @param[in,out] Data Pixel data of the channel on input; receives the
 *   wavelet transformed coefficients. This \em has to be stored with a width
 *   and height that are the next-bigger power of 2 to Width and Height given
 *   above. A suitable extension should be done on the boundary, e.g. source
 *   data is 720x512, then Width = 720 and Height = 512, but the data in
 *   Data has to be 1024x512!
 * @param[in] Approximate Flag which can be set > 0 to indicate the error
 *   computation should be more approximate. This results in less than ideal
 *   compression estimates.
 * @param[in] ReorderTable Permutation table used to reorder the wavelet
 *   coefficients. Has to have been initialized at least the dimensions of the
 *   given image. Larger tables can be reused for smaller images. Can be freed
 *   after wv_channel_compact() has returned; should be shared between
 *   multiple channels with the same (or smaller) dimensions.
 * @param[in] Progress Callback to a user-function with the progress of the
 *   compaction procedure. Can be \c NULL.
 * @param[in] UserData Userdata passed to Progress.
 * @return The compacted channel to be used in conjunction with wv_encode().
**/
t_wv_channel* wv_channel_compact(const int Width, const int Height, const unsigned char DataFormat, wv_pel* Data,
	const int Approximate, const t_reorder_table* ReorderTable, wv_progress_function Progress, void* UserData)
{
	t_wv_channel* cc = NULL;

	if (Width >= 1 && Height >= 1 && Data && ReorderTable)
	{
		wv_pel *wavelet = (Approximate == 0) ? Data : NULL;
		int width2 = 1 << wv_log2i(Width - 1); /* next bigger pow2 */
		int height2 = 1 << wv_log2i(Height - 1); /* next bigger pow2 */
		int num_levels = MAX(0, wv_log2i(MIN(width2, height2)) - 2);
		num_levels = 3;
		printf("\n-- num levels =  %d\n", num_levels);
		int cur, end; /* current and maximum value for the progress counter */
		int i, size;
		unsigned char block_bits, max_planes;

		cc = malloc(sizeof *cc);

		cc->width = Width;
		cc->height = Height;

		cc->num_blocks = 1 + 3 * num_levels; /* initial average + N * (HD + VD + DD) */
		cc->data_format = DataFormat;

		cc->block = malloc(cc->num_blocks * sizeof *cc->block);
		size = 0;
		for (i = 0; i < cc->num_blocks; i++)
		{ /* compute offsets and sizes of blocks as well as total size (for allocating reordered) */
			int shift = (cc->num_blocks - 1) / 3 - MAX(0, i - 1) / 3; /* shift is the same for i == 0 and 1-3 */
			int used_width, used_height;

			if (MIN(width2, height2) <= 2)
				shift = 0; /* only the untransformed data written as average */
			get_block_dimensions(width2, cc->width, height2, cc->height, shift, &used_width, &used_height);
			cc->block[i].offset = (i == 0) ? 0 : cc->block[i - 1].offset + cc->block[i - 1].size;
			cc->block[i].size = used_width * used_height;
			size += cc->block[i].size;
		}

		wavelet_transform(Data, width2, height2, num_levels);
		cc->reordered_channel = malloc(size * sizeof *cc->reordered_channel);
		reorder(cc->reordered_channel, Data, ReorderTable, cc->width, cc->height, 0);

		/* First quick pass to compute the number of planes and know how much work we have to do for error estimation */
		block_bits = wv_log2i(cc->num_blocks - 1); /* bits need for coding a block-# */
		max_planes = 0;
		end = 0;
		for (i = 0; i < cc->num_blocks; i++)
		{
			t_wv_block* bl = &cc->block[i];
			int j;
			wv_pel max_value;

			bl->plane = NULL;

			/* find the maximum absolute value in the block to find # of bpp */
			max_value = 0;
			for (j = bl->offset; j < bl->offset + bl->size; j++)
				max_value = MAX(max_value, abs(cc->reordered_channel[j]));
			bl->num_planes = wv_log2i(max_value);
			if (bl->num_planes > 0)
			{ /* compute size of encoded bitplanes */
				int bit_stat[sizeof (wv_pel) * 8 - 1]; /* maximum # of bitplanes (minus sign) */
				int j;

				max_planes = MAX(max_planes, bl->num_planes);
				end += (bl->num_planes + 1) * (bl->size / cc->block[0].size);
				bl->plane = malloc(bl->num_planes * sizeof *bl->plane);
				rice_encode_size(cc->reordered_channel + bl->offset, bl->size, 0, bl->num_planes, bit_stat);
				for (j = 0; j < bl->num_planes; j++)
				{
					t_wv_bitplane* bp = &bl->plane[j];

					bp->count = 1;
					bp->num_bits = (j == 0) ? bit_stat[0] : bit_stat[j] - bit_stat[j - 1];
					bp->num_bits += block_bits + (i == 0); /* add the bits needed to specify the block index (+ still-writing flag) */
				}
			}
		}
		end--; /* make it inclusive */
		cc->min_pel_store = MAX(0, wv_log2i(max_planes + 2 + 1) - 3); /* planes + 2 bits transform headroom + sign */

		/* now estimate incurred error independently for each block */
		cur = 0;
		for (i = 0; i < num_levels + 1; i++)
		{
			int bli = MAX(0, 3 * i - 2); /* block index */
			int max_planes = cc->block[bli].num_planes;
			int num = (i == 0) ? 1 : 3; /* either initial average block, or 3 blocks with HD, VD, DD */
			int work_factor = cc->block[bli].size / cc->block[0].size; /* to make progress semi-linear */
			int j, k;

			for (k = 1; k < num; k++)
				max_planes = MAX(max_planes, cc->block[bli + k].num_planes);
			if (max_planes > 0)
			{
				double mse_stat[sizeof (wv_pel) * 8][3];

				/* compute errors for those planes */
				for (j = 0; j <= max_planes; j++)
				{
					estimate_error(wavelet, width2, cc->width, height2, cc->height, num_levels + 1 - i, 1 << j, mse_stat[j]);
					if (Progress)
					{
						for (k = 0; k < num; k++)
							if (j <= cc->block[bli + k].num_planes)
								cur += work_factor;
						Progress(cur, end, UserData);
					}
				}
				/* and now "distribute" it to each of the subbands */
				for (k = 0; k < num; k++)
				{
					t_wv_block* bl = &cc->block[bli + k];

					for (j = 0; j < bl->num_planes; j++)
					{
						t_wv_bitplane* bp = &bl->plane[j];

						bp->improvement = mse_stat[bl->num_planes - j][k] - mse_stat[bl->num_planes - j - 1][k];
						bp->improvement_per_bit = bp->improvement / bp->num_bits;
					}
					agglomerate_bitplanes(bl->num_planes, bl->plane);
				}
			}
		}
		convert_planes_to_schedule(cc);
	}
	return cc;
}


/** Converts compacted channel data back to its original representation.
 * (Lossless) Inverse of wv_channel_compact().
 * \Warning The actual width and height of the free space at Data has to be the
 * next larger power of 2 of CC->width and CC->height!
 * @param[in] CC Compacted channel data (usually gotten from wv_decode()).
 * @param[in] ReorderTable Permutation table used to reorder the wavelet
 *   coefficients. Has to have been initialized with exact same width and height
 *   as given here (or an identical integer multiple of both dimensions), e.g.
 *   for our 720x512 data, we can also use a table created for 1440x1024, but
 *   not 1440x512. Should be shared between multiple channels with the same
 *   dimensions.
 * @param[out] Data Receives the original pixel data of the compacted channel.
 *   This \em has to provide space for a width and height that are the
 *   next-bigger power of 2 to CC->width and CC->height, e.g. CC is 720x512,
 *   then Data has to allocated for 1024x512!
**/
void wv_channel_uncompact(const t_wv_channel *CC, const t_reorder_table* ReorderTable, wv_pel *Data)
{
	if (CC && ReorderTable && Data)
	{
		int width2 = 1 << wv_log2i(CC->width - 1); /* next bigger pow2 */
		int height2 = 1 << wv_log2i(CC->height - 1); /* next bigger pow2 */

		/* when the dimensions are not pow2, unreorder does not write to every
		  entry in Data. This is not a problem (as the undefined coefficients do
		  not influence the area originally compressed). The wavelet transform
		  always operates on all of the data, though, so to make Valgrind happy,
		  we zero it out. */
		if (CC->width != width2 || CC->height != height2)
			memset(Data, 0, width2 * height2 * sizeof *Data);
		reorder(Data, CC->reordered_channel, ReorderTable, CC->width, CC->height, 1);
		inverse_wavelet_transform(Data, width2, height2, (CC->num_blocks - 1) / 3);
	}
}


/** Frees a compacted channel and all its associated data.
 * @param[in] CC Channel to be freed.
**/
void wv_channel_free(t_wv_channel* CC)
{
	if (CC)
	{
		free(CC->schedule);
		free(CC->block);
		free(CC->reordered_channel);
		free(CC);
	}
}


/** Gets the data format (retained data) of a channel.
 * This information was kept when encoding the channel and is set when
 * encoding a channel.
 * @param[in] CC Channel whose retained data you want to know.
 * @return User-data that was stored (8 bits).
**/
unsigned char wv_channel_get_data_format(const t_wv_channel *CC)
{
	if (CC)
		return CC->data_format;
	return 0;
}


/** Gets the width of a (usually decoded) channel.
 * @param[in] CC Channel whose width you are interested in.
 * @return Width of the channel, 0 on error
**/
int wv_channel_get_width(const t_wv_channel *CC)
{
	if (CC)
		return CC->width;
	return 0;
}


/** Gets the height of a (usually decoded) channel.
 * @param[in] CC Channel whose height you are interested in.
 * @return Height of the channel, 0 on error
**/
int wv_channel_get_height(const t_wv_channel *CC)
{
	if (CC)
		return CC->height;
	return 0;
}
