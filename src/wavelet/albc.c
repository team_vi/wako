/* albc.c, version 3.5.0

  Copyright (C) 2017 Gilson Varghese

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Gilson Varghese <gilsonvarghese7@gmail.com>

*/

#include "albc.h"

/** Advanced Lossless Binary Coding: encode the channels
 * @param[in] NumChannels # of channels in Channel.
 * @param[in] Channel pointers to t_wv_channel
**/

void albc_encode(int num_channels, t_wv_channel* Channel[]) {
	int i;
	printf("============== in albc encode =====================\n");

	/**
	 * Encode 
	*/
	for (i = 0; i < num_channels; i++)
	{

	}
}



/** Advanced Lossless Binary Coding: decode the channels
* @param[in] NumChannels # of channels in Channel.
* @param[in] Channel NumChannels pointers to t_wv_channel, which actually
**/
void albc_decode(t_wv_channel* Channel[]) {
	printf("============== in albc decode =====================\n");
}

