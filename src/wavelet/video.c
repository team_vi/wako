/** \video.c Read, Write, Encode/Decode video frame **/
/* version 3.5.0

  Copyright (C) 2001-2017 Daniel Vollmer, Gilson Varghese

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Vollmer <maven@maven.de>
  Gilson Varghese <gilsonvarghese7@gmail.com>
*/

#include <string.h>
#include <stdlib.h>
#include "video.h"
#include "io.h"
#include "utility.h"
#include "albc.h"

void prog_func(int ccur, int mmax, void* userdata)
{
	if (mmax > 0)
	{
		static int oldpercent = -1;
		int ofs = (((int)userdata & 0xffff) * 100) / ((int)userdata >> 16);
		int percent = ofs + (ccur * 100) / (mmax * ((int)userdata >> 16));

		if (percent != oldpercent)
		{
			oldpercent = percent;
			printf("%02i%%\b\b\b", percent);
			fflush(stdout);
		}
	}
}

static void raw_save(const char *fname, const wv_pel* src, const int width, const int height, const int pitch, wv_pel *dec_channel)
{
	unsigned short *tmp_row;
	// unsigned int wr_size = 0;
	int i, j;
	// printf("file %s height %d width %d\n", fname, height, width);
	tmp_row = malloc(width * sizeof *tmp_row);
	for (i = 0; i < height; i++)
	{
		for (j = 0; j < width; j++) 
		{
			tmp_row[j] = MIN(1023, MAX(0, src[i * pitch + j])); // convert to unsigned char
			dec_channel[i*width + j] = tmp_row[j];
			if(i == 779 && j == 455) {
				// printf("787,0: %d %d %d\n" , src[i * pitch + j], tmp_row[j], dec_channel[i*width + j]);
			}
		}
		// wr_size += sizeof(*tmp_row) * width;
		// fwrite(tmp_row, sizeof *tmp_row, width, out);
	}
	// printf("Write Size = %d\n", wr_size);
	free(tmp_row);
}

/**
static void print_arg(const t_cmd_arg* a)
{
	char c = tolower(a->what);

	if (c == 'i' || c == 'b')
		printf("-%-11s[-%-4s] int    (%03i) -- %s\n", a->name_long, a->name_short, *(const int *)a->thing, a->help);
	else if (c == 's' || c == 'u')
		printf("-%-11s[-%-4s] string (nil) -- %s\n", a->name_long, a->name_short, a->help);
	else
		printf("-%-11s[-%-4s] float  (0.0) -- %s\n", a->name_long, a->name_short, a->help);
}
*/

// static void print_help(int num, const t_cmd_arg a[])
// {
// 	int i;

// 	puts("Generic parameters:");
// 	for (i = 0; i < num; i++)
// 		if (isupper(a[i].what))
// 			print_arg(&a[i]);
// 	puts("Individual channel parameters:");
// 	for (i = 0; i < num; i++)
// 		if (!isupper(a[i].what))
// 			print_arg(&a[i]);
// }

// opens yuv v210 video file
FILE* open_video(char *fname, const char* mode)
{
	FILE* in;
	in = fopen(fname, mode);
	printf("openend the file\n");
	if(in == NULL)
	{
		printf("Error: Unable to open %s\n", fname);
	}
	else 
	{
		printf("Successfully opened file\n");
	}
	return in;
}

// closes yuv v210 video file
int close_video(FILE* in)
{
	fclose(in);
	return 0;
}

// reads An entire frame and pass it to split to three channels
int read_frame(FILE* in, int frame_number, yuv_frame *vid_frame, yuv_video *vid_params)
{
	char* fbuff;
	int read_length;

	fbuff = (char *)malloc(vid_params->frame_size);
	long offset = frame_number * vid_params->frame_size;
	fseek(in, offset, SEEK_SET);
	printf("file size %d\n", vid_params->frame_size);
	read_length = fread(fbuff, 1, vid_params->frame_size, in);
	fprintf(stdout, "Read %d bytes.\n", read_length);
	read_yuv(fbuff, vid_frame, vid_params);
	free(fbuff);
	return 0;
}

// reads YUV File and returns three channels
int read_yuv(char* fbuff, yuv_frame *vid_frame, yuv_video *vid_params)
{
	int word = 0;
	int yindex = 0, cbindex = 0, crindex = 0;
	int x, y;
	int i, j;
	int xpitch, ypitch, chxpitch, chypitch;
	xpitch = 1 << wv_log2i(vid_params->width - 1);
	ypitch = 1 << wv_log2i(vid_params->height - 1);
	chxpitch = 1 << wv_log2i(vid_params->chwidth - 1);
	chypitch = 1 << wv_log2i(vid_params->chheight - 1);

	printf("xpitch = %d ypitch = %d\n", xpitch, ypitch);

	vid_frame->Y  = malloc(xpitch * ypitch * sizeof(vid_frame->Y));
	vid_frame->Cb = malloc(chxpitch * chypitch * sizeof(vid_frame->Cb));
	vid_frame->Cr = malloc(chxpitch * chypitch * sizeof(vid_frame->Cr));

	printf("In read YUV \n");
	/**
	 * Read The buffer in YUV v210 format
	 * YUV v210 4:2:0 format
	 *   |**8bit**|**8bit**|**8bit**|**8bit**|
	 * 1 |00VVVVVV|VVVVYYYY|YYYYYYUU|UUUUUUUU|
	 * 2 |00YYYYYY|YYYYUUUU|UUUUUUYY|YYYYYYYY|
	 * 3 |00UUUUUU|UUUUYYYY|YYYYYYVV|VVVVVVVV|
	 * 4 |00YYYYYY|YYYYVVVV|VVVVVVYY|YYYYYYYY|
	 */
	for(y = 0; y < vid_params->height; y++)
	{

		/**
			Read Four + Four words
		*/
		for(x = 0; x < vid_params->vwidth; x+=8)
		{
			/**
				Set byte swapped integer for reading word
			*/
			word = 0;
			//printf("row = %x\n", fbuff[x + 3]);
			word = word | ((int)fbuff[(y*vid_params->vwidth) + x + 3] & 0xFF) << 24 | ((int)fbuff[(y*vid_params->vwidth) + x + 2] & 0xFF) << 16 | ((int)fbuff[(y*vid_params->vwidth) + x + 1] & 0xFF) << 8 | ((int)fbuff[(y*vid_params->vwidth) + x] & 0xFF);
			if(x == 16 && y == 0) {
				printf("%x\n", word);
			}
			vid_frame->Cb[cbindex] = ((word & (int)0x000003FF));
			vid_frame->Y[yindex] = (word & (int)0x000FFC00) >> 10;
			vid_frame->Cr[crindex] = (word & (int)0x3FF00000) >> 20;
			cbindex++;
			crindex++;
			yindex++;
			/**
				Set byte swapped integer for reading word
			*/
			word = 0;
			word = word | ((int)fbuff[(y*vid_params->vwidth) + x + 7] & 0xFF) << 24 | ((int)fbuff[(y*vid_params->vwidth) + x + 6] & 0xFF) << 16 | ((int)fbuff[(y*vid_params->vwidth) + x + 5] & 0xFF) << 8 | ((int)fbuff[(y*vid_params->vwidth) + x + 4] & 0xFF);
			if(x == 16 && y == 0) {
				printf("%x\n", word);
			}
			vid_frame->Y[yindex] = word & (int)0x000003FF;
			yindex++;
			vid_frame->Cb[cbindex] = (word & (int)0x000FFC00) >> 10;
			vid_frame->Y[yindex] = (word & (int)0x3FF00000) >> 20;
			cbindex++;
			yindex++;

			/** Second set */
			x += 8;
			/**
				Set byte swapped integer for reading word
			*/
			word = 0;
			word = word | ((int)fbuff[(y*vid_params->vwidth) + x + 3] & 0xFF) << 24 | ((int)fbuff[(y*vid_params->vwidth) + x + 2] & 0xFF) << 16 | ((int)fbuff[(y*vid_params->vwidth) + x + 1] & 0xFF) << 8 | ((int)fbuff[(y*vid_params->vwidth) + x] & 0xFF);
			vid_frame->Cr[crindex] = word & (int)0x000003FF;
			vid_frame->Y[yindex] = (word & (int)0x000FFC00) >> 10;
			vid_frame->Cb[cbindex] = (word & (int)0x3FF00000) >> 20;
			cbindex++;
			crindex++;
			yindex++;

			/**
				Set byte swapped integer for reading word
			*/
			word = 0;
			word = word | ((int)fbuff[(y*vid_params->vwidth) + x + 7] & 0xFF) << 24 | ((int)fbuff[(y*vid_params->vwidth) + x + 6] & 0xFF) << 16 | ((int)fbuff[(y*vid_params->vwidth) + x + 5] & 0xFF) << 8 | ((int)fbuff[(y*vid_params->vwidth) + x + 4] & 0xFF);
			vid_frame->Y[yindex] = word & (int)0x000003FF;
			yindex++;
			vid_frame->Cr[crindex] = (word & (int)0x000FFC00) >> 10;
			vid_frame->Y[yindex] = (word & (int)0x3FF00000) >> 20;
			crindex++;
			yindex++;
		}
		yindex+=(xpitch - vid_params->width);
		cbindex+=(chxpitch - vid_params->chwidth);
		crindex+=(chxpitch - vid_params->chwidth);
	}

	/**
		Fill the extra space
	*/
	for (i = 0; i < vid_params->height; i++)
		for (j = vid_params->width; j < xpitch; j++)
			vid_frame->Y[i * xpitch + j] = vid_frame->Y[i * xpitch + vid_params->width - 1];
	for (i = vid_params->height; i < ypitch; i++)
		memcpy(vid_frame->Y + i * xpitch, vid_frame->Y + (vid_params->height - 1) * xpitch, xpitch * sizeof *vid_frame->Y);

	for (i = 0; i < vid_params->chheight; i++)
		for (j = vid_params->chwidth; j < chxpitch; j++)
			vid_frame->Cr[i * chxpitch + j] = vid_frame->Cr[i * chxpitch + vid_params->width - 1];
	for (i = vid_params->chheight; i < chypitch; i++)
		memcpy(vid_frame->Cr + i * chxpitch, vid_frame->Cr + (vid_params->chheight - 1) * chxpitch, chxpitch * sizeof *vid_frame->Cr);

	for (i = 0; i < vid_params->chheight; i++)
		for (j = vid_params->chwidth; j < chxpitch; j++)
			vid_frame->Cb[i * chxpitch + j] = vid_frame->Cb[i * chxpitch + vid_params->width - 1];
	for (i = vid_params->chheight; i < chypitch; i++)
		memcpy(vid_frame->Cb + i * chxpitch, vid_frame->Cb + (vid_params->chheight - 1) * chxpitch, chxpitch * sizeof *vid_frame->Cb);
	return 0;
}


/**
 * Write YUV to a file
 */
int write_frame(FILE* out, int frame_number, yuv_frame *vid_frame, yuv_video * vid_params) {
	int x, y;
	int word;
	int bytes_written;
	int byte_swapped;
	int yindex = 0, crindex = 0, cbindex = 0;
	printf("In write YUV frame number %d \n", frame_number);
	/**
	 * Write The buffer in YUV v210 format
	 * YUV v210 4:2:0 format
	 *   |**8bit**|**8bit**|**8bit**|**8bit**|
	 * 1 |00VVVVVV|VVVVYYYY|YYYYYYUU|UUUUUUUU|
	 * 2 |00YYYYYY|YYYYUUUU|UUUUUUYY|YYYYYYYY|
	 * 3 |00UUUUUU|UUUUYYYY|YYYYYYVV|VVVVVVVV|
	 * 4 |00YYYYYY|YYYYVVVV|VVVVVVYY|YYYYYYYY|
	 */

	if(vid_frame->Y == NULL) 
	{
		printf("%s\n", "Empty or Null Y buffer, Unable to write to file.");
		return -1;
	}
	if(vid_frame->Cr == NULL) 
	{
		printf("%s\n", "Empty or Null Cb buffer, Unable to write to file.");
		return -1;
	}
	if(vid_frame->Cb == NULL) 
	{
		printf("%s\n", "Empty or Null Cr buffer, Unable to write to file.");
		return -1;
	}

	for(y = 0; y < vid_params->height; ++y) {
		for(x = 0; x < vid_params->width; x+=6) {
			word = 0;
			byte_swapped = 0;

			word = word | (vid_frame->Cr[crindex] & 0x3FF) << 20 | (vid_frame->Y[yindex] & 0x3FF) << 10 | (vid_frame->Cb[cbindex] & 0x3FF) << 0 ;

			if(x == 6 && y == 0) {
				printf("%x\n", word);
			}
			bytes_written = fwrite(&word, 1, sizeof(byte_swapped), out);
			//printf("bytes written = %d\n", bytes_written);

			cbindex++;
			crindex++;
			yindex++;

			word = 0;
			byte_swapped = 0;
			word = word | (vid_frame->Y[yindex+1] & 0x3FF) << 20 | (vid_frame->Cb[cbindex] & 0x3FF) << 10 | (vid_frame->Y[yindex] & 0x3FF) << 0 ;
			if(x == 6 && y == 0) {
				printf("%x\n", word);
			}
			bytes_written = fwrite(&word, 1, sizeof(byte_swapped), out);
			//printf("bytes written = %d\n", bytes_written);

			yindex++;
			cbindex++;
			yindex++;

			word = 0;
			byte_swapped = 0;
			word = word | (vid_frame->Cb[cbindex] & 0x3FF) << 20 | (vid_frame->Y[yindex] & 0x3FF) << 10 | (vid_frame->Cr[crindex] & 0x3FF) << 0 ;
			bytes_written = fwrite(&word, 1, sizeof(byte_swapped), out);
			//printf("bytes written = %d\n", bytes_written);

			cbindex++;
			crindex++;
			yindex++;

			word = 0;
			byte_swapped = 0;
			word = word | (vid_frame->Y[yindex+1] & 0x3FF) << 20 | (vid_frame->Cr[crindex] & 0x3FF) << 10 | (vid_frame->Y[yindex] & 0x3FF) << 0 ;
			bytes_written = fwrite(&word, 1, sizeof(byte_swapped), out);
			//printf("bytes written = %d\n", bytes_written);

			yindex++;
			crindex++;
			yindex++;

		}
	}
	printf("bytes written %d\n", bytes_written);
	free(vid_frame->Y);
	free(vid_frame->Cb);
	free(vid_frame->Cr);
	return 0;
}

/**
 * Write YUV to a file
 */
int write_compression_results(FILE *result, int num_frames) {
	int i;

	/**
	 * Write each compression ratio to the file
	*/
	for(i = 0; i < num_frames; ++i) {
		fprintf(result, "%.0f, ", wr_size_array[i]);
	}
	return 0;
}

/**
 *	Encode one frame using DWT 2/2(or 3/5) and Rice
 */
int encode_frame(int frame_number, yuv_frame *vid_frame, yuv_video * vid_params)
{
	printf("encode_frame: frame_number = %d\n", frame_number);
	
	printf("Encoding channel Y...\n");
	encode_channel(frame_number, "EncodeY", vid_frame->Y, vid_params->width, vid_params->height);

	printf("Encoding channel Cb...\n");
	encode_channel(frame_number, "EncodeCb", vid_frame->Cb, vid_params->width/2, vid_params->height);
	
	printf("Encoding channel Cr...\n");	
	encode_channel(frame_number, "EncodeCr", vid_frame->Cr, vid_params->width/2, vid_params->height);

	// free(vid_frame->Y);
	// free(vid_frame->Cb);
	// free(vid_frame->Cr);
	return 0;
}





/**
 *	Encode one frame using DWT 2/2(or 3/5) and Rice
 */
int encode_channel(int frame_number, char* enc_file, wv_pel* channel_data, int width, int height)
{

	printf("encode_channel: frame_number = %d\n", frame_number);

	double max_bpp = 2.0;
	t_reorder_table *reorder_table = NULL;
	t_wv_channel *channel[wv_MAX_CHANNELS + 1];
	//int decompred = 0;
	int num_channels = 0;
	int max_bits = 0;
	int error_code = 0;
	int i;
	//char dec_input[SOME_PATH_MAX] = "";
	char output[SOME_PATH_MAX] = "";
	t_channel_params channel_params[wv_MAX_CHANNELS + 1];


	// const t_cmd_arg parser[] = {
	// 	{"width", "w", "width of input", &width, 'I'},
	// 	{"height", "h", "height of input", &height, 'I'},
	// 	{"maxbits", "bits", "target rate (in total bits)", &max_bits, 'B'}, /* max_bits = 0.0 */
	// 	{"maxbpp", "bpp", "target rate (in bits/pixel)", &max_bpp, 'R'}, /* max_bpp = 0 */
	// 	{"output", "o", "greyscale raw or wko", output, 'S'},
	// 	{"decompress", "d", "decompress to output", dec_input, 'S'},
	// 	{"decompred", "dr", "size reduction (halve size N times)", &decompred, 'I'},
	// 	{"input", "i", "greyscale channel", channel_params[0].name, 'u'}, /* num_channel++ */
	// 	{"offset", "ofs", "skip bytes of (raw) input", &channel_params[0].input_offset, 'i'},
	// 	{"approx", "a", "enable more approximate error computation", &channel_params[0].approx, 'i'},
	// 	{"minpsnr", "psnr", "minimum peak signal-noise ratio", &channel_params[0].max_mse, 'p'}, /* convert to mse */
	// 	{"maxmse", "mse", "maximum mean square error", &channel_params[0].max_mse, 'f'},
	// 	{"ycocg", "y", "enable colourspace: 1 = YCoCg, 2 = YCoCg-R", &channel_params[0].ycocg, 'i'}
	// };
	max_bpp = 1.0;
	printf("Wavelet Koder %s (C) 2001-2017\n\n", wv_VERSION);

	// init to lossless
	memset(channel_params, 0, sizeof channel_params);

	// Set the channel parameters default

	channel_params[0].max_mse = 0;
	strcpy(channel_params[0].name, "Not used");
	channel_params[0].input_offset = 0;
	channel_params[0].approx = 0;
	channel_params[0].data = channel_data;
	num_channels++;

	/**
	 *	Make it encoder
	 */
	strcpy(output, enc_file);

	// for (i = 1; i < argc - 1; i++)
	// {
	// 	char* arg = argv[i];
	// 	int j;
	//
	// 	if (*arg == '/' || *arg == '-')
	// 		arg++;
	//
	// 	for (j = 0; j < (int)(sizeof parser / sizeof parser[0]); j++)
	// 	{
	// 		if (strcasecmp(arg, parser[j].name_long) == 0 || strcasecmp(arg, parser[j].name_short) == 0)
	// 		{
	// 			double a_double;
	// 			int an_int, k, mink, maxk;
	//
	// 			if (islower(parser[j].what))
	// 			{
	// 				mink = MAX(0, num_channels - 1);
	// 				maxk = wv_MAX_CHANNELS;
	// 			}
	// 			else
	// 				mink = maxk = 0;
	//
	// 			i++;
	// 			switch (parser[j].what)
	// 			{ /* BIRSuipf */
	// 				case 'B':
	// 					max_bpp = 0.0;
	// 				case 'I':
	// 				case 'i':
	// 					an_int = atoi(argv[i]);
	// 					for (k = mink; k <= maxk; k++)
	// 						*(int *)((t_channel_params *)parser[j].thing + k) = an_int;
	// 				break;
	// 				case 'R':
	// 					max_bits = 0;
	// 				case 'f':
	// 				case 'p':
	// 					a_double = atof(argv[i]);
	// 					if (parser[j].what == 'p')
	// 						a_double = wv_psnr_to_mse(a_double);
	// 					for (k = mink; k <= maxk; k++)
	// 						*(double *)((t_channel_params *)parser[j].thing + k) = a_double;
	// 				break;
	// 				case 'S':
	// 					/* no length check, bad!, I know... */
	// 					strcpy((char *)parser[j].thing, argv[i]);
	// 				break;
	// 				case 'u':
	// 					strcpy((char *)((t_channel_params *)parser[j].thing + num_channels), argv[i]);
	// 					num_channels++;
	// 			} /* switch */
	// 			break;
	// 		} /* if match */
	// 	} /* for over possible args */
	// 	if (j >= (int)(sizeof parser / sizeof parser[0]))
	// 	{
	// 		fprintf(stderr, "Unknown Parameter \"%s\"!\n", arg);
	// 		print_help(sizeof parser / sizeof parser[0], parser);
	// 		return 1;
	// 	}
	// }

	if (max_bits == 0 && max_bpp > 0.0 && width > 0 && height > 0)
		max_bits = (int)(max_bpp * width * height);


	double mse[wv_MAX_CHANNELS + 1];
	double result_mse[wv_MAX_CHANNELS + 1];
	double min_mse = DBL_MAX, max_mse = 0.0;
	int result_bits[wv_MAX_CHANNELS + 1];
	int min_bits[wv_MAX_CHANNELS + 1];
	int bits;
	int width2, height2;

	if (num_channels == 0)
	{
		fputs("No input channel(s) given!\n", stderr);
		return 1;
	}

	// for (i = 0; i < num_channels; i++)
	// { /* try to load each channel */
	// 	channel_params[i].data = raw_load(channel_params[i].name, channel_params[i].input_offset, width, height);
	// 	if (!channel_params[i].data)
	// 	{
	// 		fprintf(stderr, "Could not open input channel \"%s\"!\n", channel_params[i].name);
	// 		return 1;
	// 	}
	// }

	width2 = 1 << wv_log2i(width - 1);
	height2 = 1 << wv_log2i(height - 1);

	// go through all the channels and convert their color-space (if desired & possible)
	i = 0;
	while (i < num_channels)
	{
		if (channel_params[i].ycocg != 0)
		{
			int j, ycocg = (channel_params[i].ycocg == 2) ? 2 : 1; // actual flag

			if (num_channels - i < 3)
				ycocg = 0; // not enough channels to form colorspace
			for (j = i; j < MIN(num_channels, i + 3); j++)
				channel_params[j].ycocg = ycocg; // set for other channels of this set
			// and now actually DO IT (if we've decided we can)
			if (ycocg == 2)
			{
				printf("Converting channels [%i, %i] from RGB -> YCoCg-R...\n", i + 1, i + 3);
				wv_rgb_to_ycocgr(width2 * height2, channel_params[i+0].data,
					channel_params[i+1].data, channel_params[i+2].data);
			}
			else if (ycocg == 1)
			{
				printf("Converting channels [%i, %i] from RGB -> YCoCg...\n", i + 1, i + 3);
				wv_rgb_to_ycocg(width2 * height2, channel_params[i+0].data,
					channel_params[i+1].data, channel_params[i+2].data);
			}
			i += 3; // try looking here again
		}
		else
			i++; // try the next channel
	}

	puts("Creating reorder-table...");
	reorder_table = wv_create_reorder_table(width, height);

	printf("Compacting the channels...");
	/* init the channels */
	for (i = 0; i < num_channels; i++)
	{
		channel[i] = wv_channel_compact(width, height, channel_params[i].ycocg, channel_params[i].data,
			channel_params[i].approx, reorder_table, prog_func, (void*)((num_channels << 16) + i));
		free(channel_params[i].data); // don't need the source data anymore
		channel_params[i].data = NULL;
		mse[i] = channel_params[i].max_mse;
		min_mse = MIN(min_mse, mse[i]);
		max_mse = MAX(max_mse, mse[i]);
		min_bits[i] = 0;
	}
	if (max_bits > 0)
	{ /* we have a target bit-rate, so make all MSE targets negative / relative */
		int header_size = wv_query_header(num_channels, channel);

		for (i = 0; i < num_channels; i++)
		{
			mse[i] = -DBL_MIN - ((max_mse - min_mse) - (mse[i] - min_mse));
			if (header_size > 0 && max_bits - header_size > num_channels * 384)
				min_bits[i] = (max_bits - header_size) / (num_channels * 5); /* use minbits if we have space */
		}
	}
	puts("");

	puts("Querying the scheduler...");
	bits = wv_query_scheduler(num_channels, channel, mse, max_bits, min_bits, result_mse, result_bits); /* give a bit of info beforehand */

	if (bits >= 0)
	{
		for (i = 0; i < num_channels; i++)
			printf("%i. emse: %.03f ermse: %.03f epsnr: %.03f bits: %i\n", i + 1, result_mse[i], sqrt(result_mse[i]), wv_mse_to_psnr(result_mse[i]), result_bits[i]);
		printf("-> Achieved %.02f bits / pixel for %i data bits.\n", (double)bits / (width * height), bits);
		if (*output)
		{
			t_bit_file* bf = bit_open(output, "wb", max_bits);

			if (bf)
			{
				puts("Writing output...");
				albc_encode(num_channels, channel);
				bits = wv_encode(num_channels, channel, mse, max_bits, min_bits, bf);
				if (bits <= 0)
				{
					fputs("Could not write file!\n", stderr);
					error_code = 2;
				}
				bit_close(bf, NULL);
			}
		}
	}
	else
	{
		fputs("Could not meet the constraints!\n", stderr);
		error_code = 2;
	}

	for (i = 0; i < num_channels; i++)
		wv_channel_free(channel[i]);

	wv_free_reorder_table(reorder_table); reorder_table = NULL;

	return error_code;
}




/**
 *	Decode one frame using DWT 2/2(or 3/5) and Rice
 */
int decode_frame (int frame_number, yuv_frame *vid_frame, yuv_video * vid_params)
{
	vid_frame->Y  = malloc(vid_params->width * vid_params->height * sizeof(vid_frame->Y));
	vid_frame->Cb = malloc(vid_params->width * vid_params->height * sizeof(vid_frame->Cb));
	vid_frame->Cr = malloc(vid_params->width * vid_params->height * sizeof(vid_frame->Cr));

	printf("\n\ndecode_frame: frame_number = %d\n", frame_number);
	printf("\n\nDecoding channel Y...\n");
	decode_channel(frame_number, "EncodeY", vid_frame->Y, vid_params->width, vid_params->height);
	printf("\n\nDecoding channel Cb...\n");
	decode_channel(frame_number, "EncodeCb", vid_frame->Cb, vid_params->width/2, vid_params->height);
	printf("\n\nDecoding channel Cr...\n");
	decode_channel(frame_number, "EncodeCr", vid_frame->Cr, vid_params->width/2, vid_params->height);
	return 0;
}


/**
 *	Decode one frame using DWT 2/2(or 3/5) and Rice
 */
int decode_channel (int frame_number, char* enc_file, wv_pel *dec_channel, int width, int height)
{
	printf("decode_channel: frame_number = %d\n", frame_number);
	t_reorder_table *reorder_table = NULL;
	t_wv_channel *channel[wv_MAX_CHANNELS + 1];
	int decompred = 0;
	int num_channels = 0;
	int max_bits = 0;
	int error_code = 0;
	int i;
	char dec_input[SOME_PATH_MAX] = "";
	char output[SOME_PATH_MAX] = "";
	t_channel_params channel_params[wv_MAX_CHANNELS + 1];

	channel_params[0].max_mse = 0;
	strcpy(channel_params[0].name, "Not used");
	channel_params[0].input_offset = 0;
	channel_params[0].approx = 0;
	channel_params[0].data = NULL;
	num_channels++;
	strcpy(dec_input, enc_file);
	strcpy(output, "recreated.yuv");


	/* decompressing */
	t_bit_file* bf = bit_open(dec_input, "rb", MAX(0, max_bits));
	//t_bit_file* bf = enc_frame->Y;
	if (bf)
	{
		puts("Decoding the channels...");
		num_channels = wv_decode(NULL, decompred, channel, bf);

		if (num_channels > 0)
		{
			albc_decode(channel);
			int width2; /* next bigger pow2 */
			int height2; /* next bigger pow2 */

			/* keep for later so we can free the t_wv_channels */
			width = wv_channel_get_width(channel[0]);
			height = wv_channel_get_height(channel[0]);

			printf("width = %d height = %d\n", width, height);
			printf("width: %i height: %i channels: %i\n", width, height, num_channels);
			width2 = 1 << wv_log2i(width - 1); /* next bigger pow2 */
			height2 = 1 << wv_log2i(height - 1); /* next bigger pow2 */

			if (max_bits > 0)
				printf("Used at most %.02f bits / pixel for %i bits...\n", (double)max_bits / (width * height), max_bits);

			puts("Creating reorder-table...");
			reorder_table = wv_create_reorder_table(width, height);

			puts("Uncompacting the channels...");
			for (i = 0; i < num_channels; i++)
			{
				channel_params[i].data = malloc(width2 * height2 * sizeof *channel_params[i].data);
				channel_params[i].ycocg = wv_channel_get_data_format(channel[i]) & 3;
				wv_channel_uncompact(channel[i], reorder_table, channel_params[i].data);
				wv_channel_free(channel[i]);
			}
			wv_free_reorder_table(reorder_table);

			// go through all the channels and convert their color-space
			i = 0;
			while (i < num_channels)
			{
				if (channel_params[i].ycocg != 0)
				{
					int ycocg = channel_params[i].ycocg; // actual flag

					if (num_channels - i < 3)
						ycocg = 0; // not enough channels to form colorspace
					// and now actually DO IT (if we've decided we can)
					if (ycocg == 2)
					{
						// printf("Converting channels [%i, %i] from YCoCg-R -> RGB...\n", i + 1, i + 3);
						wv_ycocgr_to_rgb(width2 * height2, channel_params[i+0].data,
							channel_params[i+1].data, channel_params[i+2].data);
					}
					else if (ycocg == 1)
					{
						// printf("Converting channels [%i, %i] from YCoCg -> RGB...\n", i + 1, i + 3);
						wv_ycocg_to_rgb(width2 * height2, channel_params[i+0].data,
							channel_params[i+1].data, channel_params[i+2].data);
					}
					i += 3; // try looking here again
				}
				else
					i++; // try the next channel

			}

			for (i = 0; i < num_channels; i++)
			{
				if (*output)
				{
					static const char extension[wv_MAX_CHANNELS + 1] = "RGBA0123456789UVW";
					char buf[SOME_PATH_MAX];

					if (num_channels > 1)
						sprintf(buf, "%s.%c", output, extension[i]);
					else
						strcpy(buf, output);
					// printf("%s\n", "before row save");
					raw_save(enc_file, channel_params[i].data, width, height, width2, dec_channel);
					// printf("%s\n", "after row save");
					//memcpy(dec_channel, channel_params[i].data, sizeof(wv_pel) * width * height);

					if(dec_channel == NULL) {
						printf("%s\n", "Error setting dec_channel");
					} else {
						printf("%s\n", "Successfully copied to buffer");
					}
				}
				if (decompred == 0 && channel_params[i].name[0])
				{
					wv_pel* image = NULL;

					if (image)
					{
						double psnr, mse;

						psnr = wv_calc_psnr(image, channel_params[i].data, width, height, width2, &mse);
						printf("%i. mse: %.03f rmse: %.03f psnr: %.03f\n", i + 1, mse, sqrt(mse), psnr);
						free(image);
					}
				}
				free(channel_params[i].data);
			}
		}
		bit_close(bf, NULL);
	}
	else
	{
		fprintf(stderr, "Could not open compressed image \"%s\"...\n", dec_input);
		error_code = 3;
	}

	return error_code;
}

int print_results(int frame_number)
{
	fprintf(stdout, "Frame %d\n", frame_number);
	return 0;
}
