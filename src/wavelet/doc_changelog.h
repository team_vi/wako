/** \page changelog Changelog
\section ver340 Version 3.4.0, 2007-07-10
	- Overhauled the reordering-code to make the table used independent of the
	  aspect-ratio of the image. This makes old images incompatible with this
	  version of the code. The smallest dimension (in wv_create_reorder_table)
	  is now relevant for the largest table entry. Any image whose smallest
	  dimension is smaller than the one used to create the table originally can
	  safely use it.
	- We can now pass a write buffer into bit_open(), added bit_free() for
	  deallocating automatically allocated regions. Only accepts lower-case
	  mode-strings now.
	- Fixed (and simplified) scheduler preparations for very large absolute
	  target errors.
	- Added a "min bits" criterion to the scheduler, that reserves a certain
	  amount of bits for certain channels. Perceived image quality has improved
	  a fair amount, the same default values are used in Kompressor and main.c.
\section ver334 Version 3.3.4, 2007-04-19
	- Fixed a mistake in main.c that resulted in using very incorrect target-
	  MSEs for the multi-channel case. This resulted in very incorrect
	  weighing of the channels (as the target-MSE passed from main.c should
	  have been negative numbers in that case). Now all targets are treated
	  as relative when a target bit-rate is present (same as Kompressor.app).
	- Fixed a bug in io.c where very tiny negative target MSEs were
	  incorrectly converted to a non-negative number.
\section ver333 Version 3.3.3, 2006-12-29
	- Changed bit-stream. We now indicate for each channel in block 0 that
	  we're still writing with a one bit.
	- Changed semantics for wv_query_scheduler(), now the header is included,
	  also bit statistics for each channel are returned.
	- Added wv_query_header().
	- Increased size of refinement-cache by one entry.
	- Made colour-space conversion routines uses wv_pel as temporaries, not int.
	- Removed bit_write_zero() as it isn't used anymore. Also removed byte-wise
	  copying in special cases.
	- Windows compiling: Fixed mismatching declaration and implementation,
	  added M_LOG2E to channel.c.
	- Misc. cleanup.
\section ver332 Version 3.3.2, 2006-06-02
	- Fixed code to allow wv_pels up to 64bits in size.
	- Fixed forgotten cast for t_wv_fixed_point (when > int), introduced in
	  \ref ver330.
	- Simplified the file header (and in the process removed the facetious
	  attempts at backward compatibility).
	- Added config.h file for compile-time options, added asserts, made files
	  compatible between different wv_pel sizes. The config.h file contains
	  various user-changeable options like the size of a wv_pel, or the # of
	  channels supported in a single file. There are some other defines in
	  channel.h and io.c, but these should be considered fixed. If you need to
	  change these, I would like to hear about it.
	  We now store an additional variable in the header, which indicates the
	  minimum size of wv_pel needed to decode the file (from 8 bit up to 64
	  bit for each channel), which means files should be compatible
	  irrespective of settings. The asserts are enabled via their own define
	  and are disabled by default. The line on which the assert occurs also
	  contains a description of what can be done to alleviate the problem.
	- Fixed (small) error in estimate_error(), redid Mathematica-notebook.
	- Fixed fractional bit computation and initial error estimation when
	  querying the scheduler.
	- Renamed t_wv_fixed_point to wv_fixed_point for consistency.
	- Moved some rather internal defines from io.h to io.c (as io.h is now the
	  (de)coding API only).
 \section ver331 Version 3.3.1, 2006-05-29
	- Removed clipping from YCoCg-colourspace conversion functions.
	- Miscellaneous code-rearrangements (leaner / clearer code).
	- Made output-extensions conform to ImageMagick standard (.R, .G, .B).
	- Set improvement of agglomerated bitplanes to 0.0 (which allows for
	  tighter bounds for fixed point conversion) instead of adding it twice.
	- Added graph.sh for creating gnuplot rate / distortion graphs.
 \section ver330 Version 3.3.0, 2006-05-05
	- Made the fixed point error representation more flexible (with a variable
	  number of of bits used to represent the fractional part for each channel).
	  Also made it signed, to facilitate the new scheduler.
	- Renamed t_wv_channel member "retained_data" to "data_format".
	- Removed (absolute) mse and relative mse, instead we now pass an array of
	  doubles to the relevant functions, where non-negative values are absolute
	  error values, and negative ones are relative ones to the closest absolute
	  neighbour. For more details, see #wv_encode() or #wv_query_scheduler().
	  This allows the use of t_wv_channel's in multiple threads simultaneously
	  as they are essentially read-only after creation.
	- Reimplementation of the scheduler, as we longer need / have separate
	  offsets or relative MSEs. The offsets are now implicitly calculated from
	  the differences in absolute MSEs (e.g. TargetMSE of [1.0 4.0 4.0] will
	  try keep the MSE of the 1st channel 3.0 lower than the others at any
	  point in the file).
 \section ver32 Version 3.2, 2006-04-11
	- Small fixes here and there.
	- Added (but didn't default to) semi-exact error computation (i.e. transform
	  back for each quantizer in each block), which is kinda slow but more
	  accurate. The old variants are now "-approx 1" (default) and "-approx 2".
	- Removed the above again, because it's hellishly slow on larger images,
	  complicates the code, and doesn't gain that much in most usage scenarios.
	- Write each subband of each octave into their own block (i.e. no longer
	  interleaved HD, VD and DD). Increases time needed for compression (for the
	  new exact mode).
	- Changed behaviour of -bpp parameter to include all channels.
 \section ver31 Version 3.1, 2006-01-22
	- removed -decompbits parameter, now uses the value set via -maxbits/bpp
	- overhauled the bit-stream format and made it completely embedded, which
	  means we don't need a quantizer selection anymore (we simply write in the
	  scheduled order until we have filled our bit-budget).
	- made wv_MAX_CHANNELS (io.h) & wv_pel (utility.h) more flexible (e.g. can
	  write 30bit data by changing 2 defines).
	- added -bpp parameter to main.c specifying target bits per pixel
	- added 8-bit retained user-data to channel (e.g. to store type & colorspace).
	  This is now used to allow more than one set of 3 channels to have conversion.
	- added more accessors for t_wv_channel, renamed functions in channel.h.
	- bugfix: Made channel-error "offsets" (in the bit-limited case) work properly.
	- cosmetics: Documentation
 \section ver30 Version 3.0, 2006-01-13
	- made reorder-table reusable IF the table is for an exact multiple of the
	  needed dimensions.
	- renamed plenty of routines, improved symmetry between encoding / decoding
	- Reduction now rounds non-pow2 dimensions upwards
	- improved comments, reorganized code, renamed functions, used doxygen
	- implemented the YCoCg colour-space as well as its reversible cousin
	  YCoCg-R (see wv_rgb_to_ycocg() etc)
	- added an extra bit to the pre-header to indicate usage of lossless
	  colour-space conversion
	- fixed a memory leak in wv_init_multi_channels() (thanks to Nic)
	- did valgrind memcheck the code, changed some things which were incorrectly
	  detected as undefined
	- relicensed the code under the zlib / libpng license
	- changed the reordering from the Z Curve to the Hilbert (Spacefilling) Curve
 \section ver27 Version 2.7, 2003-XX-XX
	- removed MMX optimisations for wavelet transforms and made code even faster ;)
	- removed unused MaxBits parameter from wv_init_channel()
	- changed bitstream format (order in which bits are written) and removed
	  writing unneccessary zeros at the end of each block
	- changed yuv transform slightly (Cr / Cb are now centered around 0, not
	  128), as we're writing the sign in any case
	- changed colorspace conversion to be in-place
	- fixed bug in raw_load() if file was too small
	- misc optimisations to bit-files
	- added wv_ prefix to wv_log2i(), wv_mse_to_psnr(), wv_psnr_to_mse()
	- changed the # of iterations for the multi-channel size selector
	  (now bails out earlier)
	- new function to return the header of an image (wv_read_header())
	- changed layout of t_wv_dchannels
	- changed decompression to work for (hopefully) all invalid data w/o
	  overwriting anything in memory
	- wv_init_decode_channels() now accepts an extra reduction parameter to
	  return a scaled down version of the image (see -dr parameter in wako)
 \section ver26 Version 2.6, 200X-XX-XX
	- reordering now clears unused coefficients as well
	- init_channel now accepts a NEGATIVE Lossless parameter to indicate that
	  the (faster) approximate error estimation is to be used
	- optimised the wavelet-transforms (including MMX versions)
	- wrote seperate size-counter for rice-encoding
 \section ver25 Version 2.5, 200X-XX-XX
	- changed reordering again slightly, now blocks (VD, HD, DD) are transmitted
	  completely interleaved
	- reordering lowest level unnecessarily fixed
	- fixed bug with blocks consisting of all 0s
 \section ver21 Version 2.1, 200X-XX-XX
	- reordered '0' coefficients to the end (file slightly smaller)
**/
