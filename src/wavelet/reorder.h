/** \file reorder.h Data-reordering and table initialization. **/
/* version 3.4.0

  Copyright (C) 2001-2007 Daniel Vollmer

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Vollmer <maven@maven.de>

*/

#ifndef _REORDER_H_
#define _REORDER_H_

#ifdef __cplusplus // C++
extern "C" {
#endif

#include "config.h"

/** Maximum amount of reorder-levels we can possibly need (i.e. for a 32768x32768 image). **/
#define wv_NUM_REORDER_LEVELS 13

/** Reorder table which stories information needed to shuffle the data from each
 * level of the wavelet decomposition into 1D array.
**/
typedef struct {
	int num_levels; /**< Number of individual reorder tables **/
	unsigned int *level[wv_NUM_REORDER_LEVELS];
} t_reorder_table;

/** Reorder function (determines the direction). **/
typedef unsigned int (*t_reorder_function) (wv_pel *dst, wv_pel* src, const unsigned int *idx, const unsigned int num, const int width2, const unsigned int coefficient_pitch, const unsigned int xlimit, const unsigned int ylimit);

t_reorder_table* wv_create_reorder_table(const int Width, const int Height);
void wv_free_reorder_table(t_reorder_table *Table);
void get_block_dimensions(int width2, int width, int height2, int height, int shift, int *block_width, int *block_height);
void reorder(wv_pel *dst, wv_pel* src, const t_reorder_table* table, const int width, const int height, const int unreorder);

#ifdef __cplusplus // C++
}
#endif

#endif /* _REORDER_H_ */
